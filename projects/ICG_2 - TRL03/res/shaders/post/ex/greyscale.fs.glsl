#version 440
layout (location = 0) in vec2 inUV;
layout (location = 1) in vec2 inScreenCoords;

layout (location = 0) out vec4 outColor;

uniform sampler2D xImage;

void main() {
	vec4 color = texture(xImage, inUV);
	float res = (color.r + color.g + color.b) / 3.0F;

	outColor = vec4(res, res, res, color.a);
}