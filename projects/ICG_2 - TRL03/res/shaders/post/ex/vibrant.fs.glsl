// EX: filter for making a colour more vibrant
#version 440
layout (location = 0) in vec2 inUV;
layout (location = 1) in vec2 inScreenCoords;

layout (location = 0) out vec4 outColor;

uniform sampler2D xImage;

void main() {
	// color
	vec4 color = texture(xImage, inUV);
	
	// temporary clr value
	float temp = 0;
	// multiplier
	float multi = 0;

	// checking for the hgihest colour value
	if(color.r >= color.g && color.r >= color.b)
	{
		temp = 1.0F - color.r;

		// color.r * x = 1.0F
		multi = 1.0F / color.r;

	}
	else if(color.g >= color.r && color.g >= color.b)
	{
		temp = 1.0F - color.g;

		// color.g * x = 1.0F
		multi = 1.0F / color.g;
	}
	else if(color.b >= color.r && color.b >= color.g)
	{
		temp = 1.0F - color.b;

		// color.b * x = 1.0F
		multi = 1.0F / color.b;
	}

	// increasing the vibrance
	color.r = clamp(color.r * multi, 0.0F, 1.0F);
	color.g = clamp(color.g * multi, 0.0F, 1.0F);
	color.b = clamp(color.b * multi, 0.0F, 1.0F);

	outColor = color;
}