/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 01/27/2020
 * Description: implemented lights and lighting behaviour.
*/

#include "SceneBuildLayer.h"
#include "florp/game/SceneManager.h"
#include "florp/game/RenderableComponent.h"
#include <florp\graphics\MeshData.h>
#include <florp\graphics\MeshBuilder.h>
#include <florp\graphics\ObjLoader.h>
#include <glm/gtc/matrix_transform.hpp>
#include <florp\game\Transform.h>

#include "CameraComponent.h"
#include <florp\app\Application.h>
#include <SampleBehaviour.h>

// EX: LightBehaviour class
#include "ex/LightBehaviour.h"

void SceneBuilder::Initialize()
{
	using namespace florp::game;
	using namespace florp::graphics;
	
	auto* scene = SceneManager::RegisterScene("main");
	SceneManager::SetCurrentScene("main");

	MeshData data = ObjLoader::LoadObj("monkey.obj", glm::vec4(1.0f));

	Shader::Sptr shader = std::make_shared<Shader>();
	shader->LoadPart(ShaderStageType::VertexShader, "shaders/lighting.vs.glsl");
	// shader->LoadPart(ShaderStageType::FragmentShader, "shaders/blinn-phong.fs.glsl");
	shader->LoadPart(ShaderStageType::FragmentShader, "shaders/blinn-phong-multi.fs.glsl");
	// shader->LoadPart(ShaderStageType::FragmentShader, "shaders/post/ex/greyscale.fs.glsl");
	  
	shader->Link();

	Material::Sptr mat = std::make_shared<Material>(shader);
	
	// // shaders/blinn-phong.fs.glsl
	// mat->Set("a_LightPos", { 2, 0, 4 });
	// mat->Set("a_LightColor", { 1.0f, 1.0f, 1.0f });
	// mat->Set("a_AmbientColor", { 1.0f, 1.0f, 1.0f });
	// mat->Set("a_AmbientPower", 0.1f);
	// mat->Set("a_LightSpecPower", 0.5f);
	// mat->Set("a_LightShininess", 256.0f);
	// mat->Set("a_LightAttenuation", 1.0f / 100.0f);

	// mat->Set("a_Lights[0].Pos", { 2, 0, 0 });
	// mat->Set("a_Lights[0].Color", { 1.0f, 0.0f, 1.0f });
	// mat->Set("a_Lights[0].Attenuation", 1.0f / 100.0f);
	// mat->Set("a_Lights[1].Pos", { -2, 0, 0 });
	// mat->Set("a_Lights[1].Color", { 0.0f, 1.0f, 0.0f });
	// mat->Set("a_Lights[1].Attenuation", 1.0f / 100.0f);
	// mat->Set("a_EnabledLights", 2);



	// monkey
	{
		// the monkey's position
		glm::vec3 pos(0, 0, -14);

		entt::entity test = scene->CreateEntity();
		scene->AddBehaviour<SampleBehaviour>(test, glm::vec3(0.0F, 0.0F, 0.0F)); // rotates in degrees per second
		
		// light behaviour
		LightBehaviour lb(mat, 3); // enabling 2 lights

		// creating the lights
		lb.m_LightsEX[0] = Light(mat, 0, glm::vec3(4.0F, -2.0F, 0.0F), glm::vec3(1.0F, 0.0F, 0.0F), 1.0F / 100.0F);
		lb.m_LightsEX[1] = Light(mat, 1, glm::vec3(0.0F, 2.0F, 0.0F), glm::vec3(0.0F, 1.0F, 0.0F), 1.0F / 100.0F);
		lb.m_LightsEX[2] = Light(mat, 2, glm::vec3(-4.0F, -2.0F, 0.0F), glm::vec3(0.0F, 0.0F, 1.0F), 1.0F / 100.0F);

		lb.theta = 0.01F; // rotation factor of the lights (in degrees)
		scene->AddBehaviour<LightBehaviour>(test, lb); // adding the light behaviour
		
		RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(test);
		renderable.Mesh = MeshBuilder::Bake(data);
		

		renderable.Material = mat;
		Transform& t = scene->Registry().get<Transform>(test);

		t.SetPosition(pos);
	}

	{
		// used to determine the size of the application window.
		florp::app::Application* app = florp::app::Application::Get();

		// The color buffer should be marked as shader readable, so that we generate a texture for it
		RenderBufferDesc mainColor = RenderBufferDesc();
		mainColor.ShaderReadable = true;
		mainColor.Attachment = RenderTargetAttachment::Color0;
		mainColor.Format = RenderTargetType::Color24;
		// The depth attachment does not need to be a texture (and would cause issues since the format is DepthStencil)
		RenderBufferDesc depth = RenderBufferDesc();
		depth.Attachment = RenderTargetAttachment::DepthStencil;
		depth.Format = RenderTargetType::DepthStencil;
		// Our main frame buffer needs a color output, and a depth output
		FrameBuffer::Sptr buffer = std::make_shared<FrameBuffer>(app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight());
		buffer->AddAttachment(mainColor);
		buffer->AddAttachment(depth);
		buffer->Validate();


		entt::entity camera = scene->CreateEntity();
		CameraComponent& cam = scene->Registry().assign<CameraComponent>(camera);
		cam.Projection = glm::perspective(glm::radians(60.0f), 1.0f, 0.01f, 1000.0f);
		cam.Buffer = buffer;
		cam.IsMainCamera = true;
		Transform& t = scene->Registry().get<Transform>(camera);
		t.SetPosition(glm::vec3(0.0f, 0.0f, -5.0f));
	}
}
