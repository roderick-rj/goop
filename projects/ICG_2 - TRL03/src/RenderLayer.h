#pragma once
#include "florp/app/ApplicationLayer.h"

class RenderLayer : public florp::app::ApplicationLayer
{
public:
	// called when resizing the window.
	virtual void OnWindowResize(uint32_t width, uint32_t height) override;

	virtual void OnSceneEnter() override;

	// no longer needed.
	// virtual void PreRender() override;

	// Render will be where we actually perform our rendering
	virtual void Render() override; 
};
 