/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 01/10/2020
 * Description: making an OpenGL project in the new gframework, and making a hello triangle.
 * References:
*/

// INTERMEDIATE COMPUTER GRAPHICS - TUTORIAL 1
#include "GLFW/glfw3.h"
#include "glad/glad.h"
#include <iostream>


int main()
{
	// Initalize GLFW
	if (glfwInit() == GLFW_FALSE) {
		std::cout << "Failure to initalize GLFW." << std::endl;
		return 1;
	}

	// creating a GLFW window
	// GLFWwindow * = (width, height, window_name, fullscreen_bool, share_between_monitors)
	GLFWwindow* window = glfwCreateWindow(300, 300, "ICG_2 - TRL01", nullptr, nullptr); // makes the window.

	// setting the window to the current context so that the GL commands are executed to it.
	glfwMakeContextCurrent(window);

	// Initalize Glad
	// Letting glad know what function loader is being used (GL commands will be called via GLFW)
	if (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == 0) {
		std::cout << "Failed to initialize Glad." << std::endl;
		return 2;
	}

	// Displays the GPU and OpenGL versions
	std::cout << glGetString(GL_RENDERER) << std::endl;
	std::cout << glGetString(GL_VERSION) << std::endl;

	// Create VAO/VBO
	// vertices
	const GLfloat VERTS[]
	{
		-1.0f, -1.0f, 0.0f, // bottom left
		0.0f, 1.0f, 0.0f, // top centre
		1.0f, -1.0f, 0.0f // bottom right
	};

	 // colours
	 const GLfloat CLRS[]
	 {
	 	1.0f, 0.0f, 0.0f, 1.0f, // bottom left
	 	0.0f, 1.0f, 0.0f, 1.0f, // top centre
	 	0.0f, 0.0f, 1.0f, 1.0f // bottom right
	 };

	// Vertex Buffer Objects (VBO)
	GLuint posVbo = 0; // position handle
	glGenBuffers(1, &posVbo); // only using one buffer
	// specifiying the buffer, the size of the data, the data itself, and the draw mode for hte position


	 GLuint clrVbo = 1; // colour handle
	 glGenBuffers(1, &clrVbo); // only using one buffer
	// specifiying the buffer, the size of the data, the data itself, and the draw mode for the colours.
	


	// Vertex Array Objects (VAO)
	// position
	GLuint vao = 0;
	glGenVertexArrays(1, &vao); // a single vao
	glBindVertexArray(vao); // binding the vao
	//glBindBuffer(GL_ARRAY_BUFFER, posVbo);
	glBindBuffer(GL_ARRAY_BUFFER, posVbo);
	glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(GLfloat), VERTS, GL_STATIC_DRAW);
	
	// (index, size, type, normalized, stride, pointer). 
	// stride is '0' since the elments are packed close together
	
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0); // if there were other VBOs, there vertex attribute would be set here.

	// colour
	glBindBuffer(GL_ARRAY_BUFFER, clrVbo);
	glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(GLfloat), CLRS, GL_STATIC_DRAW);
	
	 glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	 glEnableVertexAttribArray(1); // enabling an attribute for colours.

	// DO NOT UNCOMMENT THIS BLOCK; it is incorrect
	// colour
	// GLuint clrVao = 0;
	// glGenVertexArrays(1, &clrVao); // a single vao
	// glBindVertexArray(clrVao); // binding the vao
	// glBindBuffer(GL_ARRAY_BUFFER, clrVbo);
	// 
	// glEnableVertexAttribArray(1); // enabling an attribute for colours.
	// glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, NULL);



	// Create shaders
	 // NOTE: shaders prefer it when names are consistent between the vertex shader and the fragment shader.
	 // this is why fsClr is named the way it is. Changing it to inClr/outClr caused it to crash.

	// Vertex Shader (with colour)
	const char* vertexShader =
		"#version 400\n" // version we're using
		"in vec3 inPos;" // input - position
		"in vec4 inClr;"
		"out vec4 fsClr;"
		"void main() {"
		"	gl_Position = vec4(inPos, 1.0);" // gl_Position is internal to the shader. This adds the forth position for the vec4.
		"	fsClr = inClr;"
		"}";

	// Vertex Shader (without color)
	//const char* vertexShader =
	//	"#version 400\n" // version we're using
	//	"in vec3 inPos;" // input - position
	//	"void main() {"
	//	"	gl_Position = vec4(inPos, 1.0);" // gl_Position is internal to the shader. This adds the forth position for the vec4.
	//	"}";

	// Fragment Shader
	const char* fragmentShader =
		"#version 400\n"
		"in vec4 fsClr;"
		"out vec4 outClr;"
		"void main() {"
		"	outClr = fsClr;"
		"}";

	// Fragment Shader
	//const char* fragmentShader =
	//	"#version 400\n"
	//	"out vec4 outClr;"
	//	"void main() {"
	//	"	outClr = vec4(0.5, 0.5, 0.5, 1.0);"
	//	"}";

	// Loading the Shaders
	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	// this is where you would load it in from a file if you had one.
	// since it's a length of string it's only a value of 1.
	glShaderSource(vs, 1, &vertexShader, NULL); // (GLuint, amount, file, length)
	glCompileShader(vs);

	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	// this is where you would load it in from a file if you had one.
	// since it's a length of string it's only a value of 1.
	glShaderSource(fs, 1, &fragmentShader, NULL); // (GLuint, amount, file, length)
	glCompileShader(fs);

	// creating the shader program
	GLuint shader_program = glCreateProgram();
	glAttachShader(shader_program, vs);
	glAttachShader(shader_program, fs);
	glLinkProgram(shader_program);

	// while window is open
	while (!glfwWindowShouldClose(window)) {
		// Poll the Window
		// Polling window events (clicks, keypressed, closing, etc.)
		glfwPollEvents();

		// Clear our screen every frame
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f); // ClearColour(R, G, B, A) 
		glClear(GL_COLOR_BUFFER_BIT);
		
		// Bind the VAO
		glUseProgram(shader_program); // telling GL to use the shader program.
		
		// if you have more vao, you find the different subset of vao and call the draw call.
		glBindVertexArray(vao); // binding the vao

		// Draw Stuff
		// It takes the primitive type, initial position in the buffer, and then the amount we want to draw.
		glDrawArrays(GL_TRIANGLES, 0, 3); // you can also use DrawElement when you have multiple objects.
		
		// Swap Buffers
		// swapping the buffers
		glfwSwapBuffers(window);
	}

	// delete everything
	// delete window;
	// delete vertexShader;
	// delete fragmentShader;
	return 0;
}