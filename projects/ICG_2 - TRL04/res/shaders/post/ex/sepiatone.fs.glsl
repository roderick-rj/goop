// EX: a shader for a sepia tone filter
#version 440
layout (location = 0) in vec2 inUV;
layout (location = 1) in vec2 inScreenCoords;

layout (location = 0) out vec4 outColor;

uniform sampler2D xImage;

void main() {
	// sepia tone maximum colour. RGB = (112, 66, 20)
	vec3 sepiaColor = vec3(0.43921568627F, 0.25882352941F, 0.07843137254F);

	// the pixel colour
	vec4 color = texture(xImage, inUV);
	
	// result
	// a percentage of the sepia tone colour is calculated based on the original rgb values in reference to the white value of (1, 1, 1);
	vec3 result = vec3(sepiaColor.r * color.r, sepiaColor.g * color.g, sepiaColor.b * color.b);

	outColor = vec4(result, color.a);
}