/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 01/27/2020
 * Description: a light class to add lights to a material.
*/
// EX: for the takehome assi
#pragma once

#include <GLM/vec2.hpp>
#include <GLM/vec3.hpp>
#include <GLM/vec4.hpp>
#include <florp/game/Material.h>

#include <string>

// light
struct Light
{
public:
    Light() = default;

    Light(florp::game::Material::Sptr mat, int index, glm::vec3 pos, glm::vec3 clr, float atten)
    {
        material = mat;
        SetIndex(index);
        SetPosition(pos);
        SetColor(clr);
        SetAttenuation(atten);
    }

    // gets the index
    int GetIndex() const { return index; }

    // sets the index
    void SetIndex(int index) 
    { 
        if(index >= 0 && index < MAX_LIGHTS)
            this->index = index; 
    };

    // gets the position
    glm::vec3 GetPosition() const { return position; }

    // sets the position
    void SetPosition(glm::vec3 newPos)
    {
        position = newPos; 

        if (material != nullptr)
            material->Set("a_Lights[" + std::to_string(index) + "].Pos", position);
    }

    // gets the color
    glm::vec3 GetColor() const { return color; }

    // sets the color
    void SetColor(glm::vec3 newClr)
    {
        color = newClr;

        // mat->Set("a_Lights[0].Color", { 1.0f, 0.0f, 1.0f });
        if (material != nullptr)
            material->Set("a_Lights[" + std::to_string(index) + "].Color", color);

    }

    // gets the attenuation
    float GetAttenuation() const { return attenuation; }

    // sets the attenuation
    void SetAttenuation(float atten) 
    { 
        attenuation = atten; 

        if (material != nullptr)
            material->Set("a_Lights[" + std::to_string(index) + "].Attenuation", atten);
    }

    // translates the light
    void Translate(glm::vec3 translate) { SetPosition(position + translate); }

    // the material
    florp::game::Material::Sptr material = nullptr;
    
    const static int MAX_LIGHTS = 16;
private:
    glm::vec3 position; // (x, y, z)
    glm::vec3 color; // rgb
    float attenuation = 1.0F;
    int index = 0; // the light's index.
    
};
