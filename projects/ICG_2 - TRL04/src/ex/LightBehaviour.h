/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 01/27/2020
 * Description: behaviour for moving lights around.
*/

// EX: the behaviour ofr the lighting
#pragma once
#include "florp/game/IBehaviour.h"
#include "Logging.h"
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"
#include "imgui.h"

#include <iostream>
#include <florp/game/Material.h>
#include <ex/Light.h>

// lighting behaviour
class LightBehaviour : public florp::game::IBehaviour {
public:
	LightBehaviour(florp::game::Material::Sptr & mat, int enabledLights) : IBehaviour(), m_EnabledLights(enabledLights) 
	{
		mat->Set("a_EnabledLights", m_EnabledLights);
	};

	virtual ~LightBehaviour() = default;

	// TODO: change lighting behaviour.
	virtual void Update(entt::entity entity) override {
		auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
		
		glm::mat4 posMatEX = glm::mat4(1.0F); // position matrix
		glm::mat4 resMatEX; // resulting matrix

		// updating the lights
		for (int i = 0; i < m_EnabledLights; i++)
		{
			// filling the position matrix
			posMatEX[0][3] = m_LightsEX[i].GetPosition().x;
			posMatEX[1][3] = m_LightsEX[i].GetPosition().y;
			posMatEX[2][3] = m_LightsEX[i].GetPosition().z;
			posMatEX[3][3] = 1;

			// rotating the lights
			resMatEX = glm::rotate(posMatEX, glm::radians(theta), { 0.0F, 0.0F, 1.0F });
			
			// setting the new position
			m_LightsEX[i].SetPosition(
				glm::vec3(
					resMatEX[0][3],
					resMatEX[1][3],
					resMatEX[2][3]
				)
			);

			// reseting the matrices
			posMatEX = glm::mat4(1.0F);
			resMatEX = glm::mat4(1.0F);
			// std::cout << "SceneBuilder\n";
		}
	}

	// rotation factor (in degrees)
	float theta = 0;

	// light array
	Light m_LightsEX[16];

private:
	// amount of enabled lights
	int m_EnabledLights = 0;
};