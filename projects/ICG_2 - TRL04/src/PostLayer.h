#pragma once
#include "florp/app/ApplicationLayer.h"
#include "FrameBuffer.h"
#include "florp/graphics/Mesh.h"
#include "florp/graphics/Shader.h"

class PostLayer : public florp::app::ApplicationLayer
{
public:
	PostLayer();
	virtual void OnWindowResize(uint32_t width, uint32_t height) override;
	virtual void PostRender() override;

protected:
	FrameBuffer::Sptr myMainFrameBuffer;
	florp::graphics::Mesh::Sptr myFullscreenQuad;

	struct PostPass {
		typedef std::shared_ptr<PostPass> Sptr;
		florp::graphics::Shader::Sptr Shader;
		FrameBuffer::Sptr Output;
		
		struct Input {
			// first parameter
			Sptr Pass;

			// second parameter
			RenderTargetAttachment Attachment = RenderTargetAttachment::Color0;
			
			// third parameter
			bool UsePrevFrame = false; 
		};

		std::vector<Input> Inputs;
		
		// scale framebuffer based on screen size
		float ResolutionMultiplier = 1.0f;
	};
	
	std::vector<PostPass::Sptr> myPasses;
	PostPass::Sptr __CreatePass(const char* fragmentShader, float scale = 1.0f) const;

	// std::vector<PostPass> myPasses;
};
