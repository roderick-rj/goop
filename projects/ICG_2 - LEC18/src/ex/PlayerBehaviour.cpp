#include "PlayerBehaviour.h"
#include "florp/app/Window.h"
#include "florp/app/Application.h"

void PlayerBehaviour::Update(entt::entity entity)
{
	using namespace florp::app;

	auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
	Window::Sptr window = Application::Get()->GetWindow();
	float deltaTime = Timing::DeltaTime;

	glm::vec3 translation{};

	if (player1) // player 1 control set
	{
		// Input
		if (window->IsKeyDown(Key::A))  // Left
		{
			translation.x += moveSpeed * deltaTime;
		}
		else if (window->IsKeyDown(Key::D))	// Right
		{
			translation.x -= moveSpeed * deltaTime;
		}
	}
	else // player 2 control set
	{
		// Input
		if (window->IsKeyDown(Key::Left))  // Left
		{
			translation.x += moveSpeed * deltaTime;
		}
		else if (window->IsKeyDown(Key::Right))	// Right
		{
			translation.x -= moveSpeed * deltaTime;
		}
	}

	// inverted
	translation *= -1;

	transform.SetPosition(transform.GetLocalPosition() + translation);
}
