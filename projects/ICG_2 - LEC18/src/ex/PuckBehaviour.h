#pragma once
#include "florp/game/IBehaviour.h"
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"
#include <GLM/glm.hpp>
#include <random>

class PuckBehaviour : public florp::game::IBehaviour {
public:
	PuckBehaviour(glm::vec3 start) : IBehaviour(), startingPos(start)
	{
		// y is up and down (i.e. above and below floor)
		direc = glm::vec3(rand() % 3 - 1.0F, 0.0F, rand() % 3 - 1.0F);
		
		// checks for if the puck is moving at all
		if (direc == glm::vec3(0, 0, 0))
			direc.x = 1.0F;
	}

	virtual ~PuckBehaviour() = default;

	virtual void Update(entt::entity entity) override;

	// direction on the x, y, and z-axis
	static glm::vec3 direc;

private:
	// starting position
	glm::vec3 startingPos;

	// movement speed
	static float moveSpeed;

	// bounds for position
	static const glm::vec3 LOWER_BOUNDS;
	static const glm::vec3 UPPER_BOUNDS;

protected:
};
