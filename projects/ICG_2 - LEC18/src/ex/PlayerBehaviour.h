#pragma once
#include "florp/game/IBehaviour.h"
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"
#include <GLM/glm.hpp>

class PlayerBehaviour : public florp::game::IBehaviour {
public:
	// true if p1, false if p2.
	PlayerBehaviour(bool isPlayer1) : IBehaviour(), player1(isPlayer1) {}

	PlayerBehaviour(bool isPlayer1, float moveSpeed) : IBehaviour(), player1(isPlayer1), moveSpeed(moveSpeed) {}

	virtual ~PlayerBehaviour() = default;

	virtual void Update(entt::entity entity) override;

private:
	// determines whether to use the p1 or p2 control set
	bool player1 = true;

	float moveSpeed = 20.0F;

	float radius = 1.0F;

};


