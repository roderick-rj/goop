#include "PuckBehaviour.h"
#include "florp/app/Window.h"
#include "florp/app/Application.h"

// direction
glm::vec3 PuckBehaviour::direc = glm::vec3(1, 0, 1);

// movement speed
float PuckBehaviour::moveSpeed = 5.0F;

// movement bounds
const glm::vec3 PuckBehaviour::LOWER_BOUNDS = glm::vec3(-500.0F, -500.0F, -500.0F);
const glm::vec3 PuckBehaviour::UPPER_BOUNDS = glm::vec3(500.0F, 500.0F, 500.0F);

// puck behaviour update
void PuckBehaviour::Update(entt::entity entity)
{
	using namespace florp::app;

	auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
	Window::Sptr window = Application::Get()->GetWindow();

	// bounds checking
	{
		glm::vec3 pos = transform.GetLocalPosition();
		glm::vec3 temp = pos;

		temp = glm::clamp(temp, LOWER_BOUNDS, UPPER_BOUNDS);

		// reset to start
		if (temp != pos)
		{
			transform.SetPosition(startingPos);

			// direction change
			if (rand() % 2 == 1)
				direc.x *= -1;

			if (rand() % 2 == 1)
				direc.z *= -1;
		}
	}

	// calculating translation
	glm::vec3 translation{ direc * moveSpeed * Timing::DeltaTime };

	transform.SetPosition(transform.GetLocalPosition() + translation);
}
