#pragma once

#pragma once
#include "florp/app/ApplicationLayer.h"
#include "entt.hpp"

class CollisionLayer : public florp::app::ApplicationLayer
{
public:
	CollisionLayer(entt::entity& p1, entt::entity& p2, entt::entity puck) : e1(p1), e2(p2), e3(puck)
	{}

	void CollisionLayer::Update();

private:
	entt::entity e1;
	entt::entity e2;
	entt::entity e3;

	// radius for players nadp uck
	const float PLAYER_RADIUS = 5.0F;
	const float PUCK_RADIUS = 2.5F;
protected:

};
