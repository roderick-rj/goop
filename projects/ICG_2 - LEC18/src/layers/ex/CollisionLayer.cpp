#include "CollisionLayer.h"

#include "florp/game/IBehaviour.h"
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"
#include "florp/app/Window.h"
#include "florp/app/Application.h"
#include "ex/PuckBehaviour.h"
#include <GLM/glm.hpp>

// update loop
void CollisionLayer::Update()
{
	using namespace florp::app;

	auto& p1 = CurrentRegistry().get<florp::game::Transform>(e1);
	auto& p2 = CurrentRegistry().get<florp::game::Transform>(e2);
	auto& puck = CurrentRegistry().get<florp::game::Transform>(e3);

	Window::Sptr window = Application::Get()->GetWindow();
	float deltaTime = Timing::DeltaTime;

	glm::vec3 p1Pos = p1.GetLocalPosition();
	glm::vec3 p2Pos = p2.GetLocalPosition();
	glm::vec3 puckPos = puck.GetLocalPosition();

	// player 1 collision
	if (PLAYER_RADIUS + PUCK_RADIUS > (puckPos - p1Pos).length())
	{
		PuckBehaviour::direc *= -1;
	}
	// player 2 colision
	else if (PLAYER_RADIUS + PUCK_RADIUS > (puckPos - p2Pos).length())
	{
		PuckBehaviour::direc *= -1;
	}
}
