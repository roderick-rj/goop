#pragma once
#include "florp/game/IBehaviour.h"
#include <GLM/glm.hpp>
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"
#include "florp/app/Window.h"
#include "florp/app/Application.h"
#include "AudioEngine.h"


class AudioMovementBehaviour : public florp::game::IBehaviour {
public:
	AudioMovementBehaviour() : IBehaviour(),
		newPosition(glm::vec3(15,0,-5)),
		delay(0)
	{};
	virtual ~AudioMovementBehaviour() = default;


	virtual void OnLoad(entt::entity entity) override {
		auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);

		AudioEngine& audioEngine = audioEngine.GetInstance();

		audioEngine.LoadEvent("Enemy hit");
		audioEngine.LoadEvent("Enemy Score");
		audioEngine.LoadEvent("Lose");
		audioEngine.LoadEvent("Paddle hit");
		audioEngine.LoadEvent("Score");
		audioEngine.LoadEvent("Start");
		audioEngine.LoadEvent("Wall hit");
		audioEngine.LoadEvent("Win");

		audioEngine.PlayEvent("Start");
	}

	virtual void Update(entt::entity entity) override {
		using namespace florp::app;
		auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
		Window::Sptr window = Application::Get()->GetWindow();
		delay += 1 * florp::app::Timing::DeltaTime;

		if (false) 
		{
			AudioEngine::GetInstance().PlayEvent(" ");
		}

	}

private:
	float delay;
	glm::vec3 newPosition;
};