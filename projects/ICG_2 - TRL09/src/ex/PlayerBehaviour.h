#pragma once
#include "florp/game/IBehaviour.h"
#include <GLM/glm.hpp>

class PlayerBehaviour: public florp::game::IBehaviour {
public:
	PlayerBehaviour();

	virtual ~PlayerBehaviour() = default;

	virtual void Update(entt::entity entity) override;

private:
	// speed
	glm::vec3 speed = glm::vec3(10.0F);

};


