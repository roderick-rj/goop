#include "PlayerBehaviour.h"

#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"
#include "florp/app/Window.h"
#include "florp/app/Application.h"

PlayerBehaviour::PlayerBehaviour() : IBehaviour()
{
	speed = glm::vec3(10.0F);
}

void PlayerBehaviour::Update(entt::entity entity)
{
	using namespace florp::app;

	auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
	Window::Sptr window = Application::Get()->GetWindow();
	const float dt = Timing::DeltaTime;
	
	// translation vector
	glm::vec3 tlate(0);

	// Input - movement
	if (window->IsKeyDown(Key::A) || window->IsKeyDown(Key::Left))  // Left
	{
		tlate.x += speed.x * dt;
	}
	else if (window->IsKeyDown(Key::D) || window->IsKeyDown(Key::Right))	// Right
	{
		tlate.x -= speed.x * dt;
	}

	transform.SetPosition(transform.GetLocalPosition() + tlate);
}