#pragma once
#include "florp/game/IBehaviour.h"
#include <GLM/glm.hpp>

class EnemyBehaviour : public florp::game::IBehaviour {
public:
	EnemyBehaviour(float a_fallSpeed, float a_strafeSpeed, glm::vec3 a_range, glm::vec3 a_direc);

	virtual ~EnemyBehaviour() = default;

	virtual void Update(entt::entity entity) override;

private:
	// speed
	float fallSpeed = 1.0F;
	float strafeSpeed = 1.0F;

	// range of straf, and direction of movement.
	glm::vec3 direc;
	glm::vec3 range;

	
	// t value
	float t = 0.0F;
	// strafe direction
	bool strafe_dir_f = true;
};


