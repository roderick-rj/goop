#include "EnemyBehaviour.h"

#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"
#include "florp/app/Window.h"
#include "florp/app/Application.h"

// enemy behaviour
EnemyBehaviour::EnemyBehaviour(float a_fallSpeed, float a_strafeSpeed, glm::vec3 a_range, glm::vec3 a_direc) :
	IBehaviour()
{
	fallSpeed = a_fallSpeed;
	strafeSpeed = a_strafeSpeed;
	range = a_range;
	direc = glm::normalize(a_direc);
}

void EnemyBehaviour::Update(entt::entity entity)
{
	using namespace florp::app;

	auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
	Window::Sptr window = Application::Get()->GetWindow();
	const float dt = Timing::DeltaTime;

	// new position
	glm::vec3 newPos = transform.GetLocalPosition();

	// strafe
	if(false) // commented out because it made objects disappear
	{
		// line of movement
		glm::vec3 lineStart = transform.GetWorldPosition() - range / 2.0F;
		glm::vec3 lineEnd = transform.GetWorldPosition() + range / 2.0F;

		t += strafeSpeed * dt;
		t = glm::clamp(t, 0.0F, 1.0F);

		// checks strafe direction (forward or backward)
		if(strafe_dir_f)
			newPos = mix(lineStart, lineEnd, t);
		else
			newPos = mix(lineEnd, lineStart, t);

		if (t == 1.0F)
		{
			t = 0.0F;
			strafe_dir_f = !strafe_dir_f; // reverse direction.
		}
	}

	// movement down.
	newPos += direc * fallSpeed * dt; 

	transform.SetPosition(newPos);
}