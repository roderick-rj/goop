#version 410

layout(location = 3) in vec2 inUV;

layout(location = 0) out vec4 outColor;

uniform sampler2D xImage;

// lut sampler
uniform sampler3D a_Sampler;

void main() {
	// Write the output
	outColor = texture3D(a_Sampler, texture(xImage, inUV).rgb);
}