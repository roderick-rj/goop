#version 440
layout (location = 0) in vec2 inUV;
layout (location = 1) in vec2 inScreenCoords;

layout (location = 0) out vec4 outColor;

uniform sampler2D xImage;

// adjustable gamma
uniform float a_Gamma;

void main() {
	// color
	vec4 color = texture(xImage, inUV);

	// gamma correction default.
	float gamma = (a_Gamma > 0.0F) ? a_Gamma : 2.20F;

	outColor = vec4(pow(color.rgb, vec3(1.0F/gamma)), 1.0F);
}