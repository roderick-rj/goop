#include "TestSceneLayer.h"

// on-scene enter
void TestSceneLayer::OnSceneEnter()
{
	ApplicationLayer::OnSceneEnter();
}

void TestSceneLayer::OnSceneExit()
{
	ApplicationLayer::OnSceneExit();
}
