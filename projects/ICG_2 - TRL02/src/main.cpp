/*
 * Name: Roderick "R.J." Montague
 *  :
 */
// INTERMEDIATE COMPUTER GRAPHICS - TUTORIAL 02

#include <florp/app/Application.h>
// #include <florp/graphics/>
#include <florp/game/SceneManager.h>
// #include <florp/game/Scene.h>
#include <florp/game/BehaviourLayer.h>
// #include <florp/app/ApplicationLayer.h>
#include <florp/game/ImGuiLayer.h>

#include "SceneBuilder.h"
#include "RenderLayer.h"
#include "TestSceneLayer.h"

int main()
{
    // create our application
    florp::app::Application* app = new florp::app::Application();

    // Set up our layers
    // app->AddLayer<florp::game::BehaviourLayer>();
    app->AddLayer<SceneBuilder>();
    app->AddLayer<florp::game::ImGuiLayer>();

    app->Run();

    return 0;
}