#pragma once

#include <florp/app/ApplicationLayer.h>
#include <CameraComponent.h>
#include <glm/glm.hpp>

class RenderLayer : public florp::app::ApplicationLayer
{
public:
	RenderLayer(glm::vec4 a_ClearColor = glm::vec4(0.3, 0.2, 0.9, 1.0));

	// pre render

	// render draw

	// for application to perform events before all rendering.
	void PreRender();

	// for application to render entities
	void Render();

	// for the application to perform events after rendering is finished.
	void PostRender();


	glm::vec4 myClearColor;
	CameraComponent camera;
private:
protected:

};