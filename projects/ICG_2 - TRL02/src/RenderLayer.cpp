#include "RenderLayer.h"

#include <glad/glad.h>
#include <entt.hpp>

#include<florp/graphics/MeshBuilder.h>
#include<florp/game/RenderableComponent.h>
#include<florp/game/SceneManager.h>

// constructor
RenderLayer::RenderLayer(glm::vec4 a_ClearColor)
	: myClearColor(a_ClearColor)
{
}

// pre-rendering
void RenderLayer::PreRender()
{
	ApplicationLayer::PreRender();
}

// rendering entities
void RenderLayer::Render()
{
	ApplicationLayer::Render();

	// Clear our new inset area with the scene clear color
	glClearColor(myClearColor.x, myClearColor.y, myClearColor.z, myClearColor.w);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// We'll grab a reference to the ecs to make things easier
	auto& ecs = CurrentRegistry();

	
	// ecs.sort<>([&](const florp::game::RenderableComponent& lhs, const florp::game::RenderableComponent& rhs) {
	// 	if (rhs.Material == nullptr || rhs.Mesh == nullptr)
	// 		return false;
	// 	else if (lhs.Material == nullptr || lhs.Mesh == nullptr)
	// 		return true;
	// 	// else if (lhs.Material->HasTransparency & !rhs.Material->HasTransparency) //
	// 	// 	return false; // This section is new
	// 	// else if (!lhs.Material->HasTransparency & rhs.Material->HasTransparency) // The order IS important
	// 	// 	return true; //
	// 	else if (lhs.Material->GetShader() != rhs.Material->GetShader())
	// 		return lhs.Material->GetShader() < rhs.Material->GetShader();
	// 	else
	// 		return lhs.Material < rhs.Material;
	// 	});
	// 
	// // moved to be above the draw calls for the other objects so that depth buffering works properly.
	// auto scene = CurrentScene();
	// // Draw the skybox after everything else, if the scene has one
	// 
	// // These will keep track of the current shader and material that we have bound
	// florp::game::Material::Sptr mat = nullptr;
	// florp::graphics::Shader::Sptr boundShader = nullptr;
	// // A view will let us iterate over all of our entities that have the given component types
	// auto view = ecs.view<florp::game::RenderableComponent>();
	// 
	// for (const auto& entity : view) {
	// 	// Get our shader
	// 	const florp::game::RenderableComponent& renderer = ecs.get<florp::game::RenderableComponent>(entity);
	// 	// Early bail if mesh is invalid
	// 	if (renderer.Mesh == nullptr || renderer.Material == nullptr)
	// 		continue;
	// 	// If our shader has changed, we need to bind it and update our frame-level uniforms
	// 	if (renderer.Material->GetShader() != boundShader) {
	// 		boundShader = renderer.Material->GetShader();
	// 		boundShader->Bind();
	// 		boundShader->SetUniform("a_CameraPos", camera->GetPosition());
	// 		boundShader->SetUniform("a_Time", static_cast<float>(glfwGetTime())); // passing in the time.
	// 	}
	// 	// If our material has changed, we need to apply it to the shader
	// 	if (renderer.Material != mat) {
	// 		mat = renderer.Material;
	// 		mat->Apply();
	// 	}
	// 
	// 	// We'll need some info about the entities position in the world
	// 	const TempTransform& transform = ecs.get_or_assign<TempTransform>(entity);
	// 	// Get the object's transformation
	// 	glm::mat4 worldTransform = transform.GetWorldTransform();
	// 	// Our normal matrix is the inverse-transpose of our object's world rotation
	// 	// Recall that everything's backwards in GLM
	// 	glm::mat3 normalMatrix = glm::mat3(glm::transpose(glm::inverse(worldTransform)));
	// 
	// 	// Update the MVP using the item's transform
	// 	mat->GetShader()->SetUniform(
	// 		"a_ModelViewProjection",
	// 		camera->GetViewProjection() *
	// 		worldTransform);
	// 	// Update the model matrix to the item's world transform
	// 	mat->GetShader()->SetUniform("a_Model", worldTransform);
	// 	// Update the model matrix to the item's world transform
	// 	mat->GetShader()->SetUniform("a_NormalMatrix", normalMatrix);
	// 
	// 	// TODO: add ability to turn face culling on and off for a given object
	// 	// draws the item
	// 	if (renderer.Mesh->IsVisible())
	// 	{
	// 		// if the mesh is in wireframe mode, and the draw call isn't set to that already.
	// 		if (renderer.Mesh->IsWireframe() != wireframe)
	// 		{
	// 			wireframe = !wireframe;
	// 
	// 			// switches between wireframe mode and fill mode.
	// 			(wireframe) ? glPolygonMode(GL_FRONT_AND_BACK, GL_LINE) : glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	// 		}
	// 
	// 		renderer.Mesh->Draw();
	// 	}
	// }

}

// post-render
void RenderLayer::PostRender()
{
	ApplicationLayer::PostRender();
}
