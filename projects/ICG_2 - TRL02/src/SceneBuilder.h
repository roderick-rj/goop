#pragma once
// SceneBuilder

#include <florp/app/ApplicationLayer.h>

// SceneBuildLayer
class SceneBuilder : public florp::app::ApplicationLayer
{
public:
	// load content from last semester
	void Initialize();

private:
protected:
};
