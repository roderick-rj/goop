#pragma once

#include <florp/app/ApplicationLayer.h>

class TestSceneLayer : public florp::app::ApplicationLayer
{
public:
	void Update() override;
	void LateUpdate() override;
	void FixedUpdate() override;
	void RenderGUI() override;
	
	
	void OnSceneEnter() override;
	
	void OnSceneExit() override;

private:
protected:

};