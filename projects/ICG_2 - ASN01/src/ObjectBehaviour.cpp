/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: object behaviour class.
 * References: none
*/

#include "ObjectBehaviour.h"

#include <florp/app/Timing.h>
#include <florp/game/Transform.h>
#include <glm/mat4x4.hpp>
#include "Object.h"

// constructor
ObjectBehaviour::ObjectBehaviour() : IBehaviour()
{
}

// setting the values
ObjectBehaviour::ObjectBehaviour(glm::vec3 aPos, glm::vec3 aOrin, glm::vec3 aScale, glm::vec3 aRot)
{
	// saving the values
	position = aPos;
	orientation = aOrin;
	scale = aScale;
	rotationFactor = aRot;
}

// update loop
void ObjectBehaviour::Update(entt::entity entity)
{
	// delta time and transformation.
	float deltaTime = florp::app::Timing::DeltaTime;
	auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);

	// changing the orientation
	orientation += glm::vec3(
		rotationFactor.x * deltaTime, 
		rotationFactor.y * deltaTime, 
		rotationFactor.z * deltaTime);

	// was originally going to model rotations after those found in the light behaviour class, but I decided against it.
	// glm::mat4 posMat = glm::mat4(1.0F); // position matrix
	// glm::mat4 resMat; // resulting matrix
	// 
	// // filling the position matrix
	// posMat[0][3] = position.x;
	// posMat[1][3] = position.y;
	// posMat[2][3] = position.z;
	// posMat[3][3] = 1;

	// resMat = glm::rotate(posMat, glm::radians(rotationFactor.x), { 0.0F, 0.0F, 1.0F });

	// rotating the lights
	// resMat = glm::rotate(posMat, orientation.x, { 0.0F, 0.0F, 1.0F });
	// 
	// resMat = glm::rotate(posMat, glm::radians(orientation.y), glm::vec3(0.0F, 1.0F, 0.0F));
	// resMat = glm::rotate(posMat, glm::radians(orientation.z), glm::vec3(0.0F, 0.0F, 1.0F));

	// position = glm::vec3(resMat[0][3], resMat[1][3], resMat[2][3]);

	// transformations
	transform.SetPosition(position);
	transform.SetEulerAngles(orientation);
	transform.SetScale(scale);
}
