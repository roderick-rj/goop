/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: builds objects for the scene.
 * References: none
*/

#include "SceneBuildLayer.h"
#include "florp/game/SceneManager.h"
#include "florp/game/RenderableComponent.h"
#include <florp\graphics\MeshData.h>
#include <florp\graphics\MeshBuilder.h>
#include <florp\graphics\ObjLoader.h>
#include <glm/gtc/matrix_transform.hpp>
#include <florp\game\Transform.h>

#include "CameraComponent.h"
#include <florp\app\Application.h>
#include <SampleBehaviour.h>

// new for the project.
#include "ObjectBehaviour.h"
#include "LightBehaviour.h"
#include "LookUpTable.h"
#include "Texture3D.h"
#include "CameraBehaviour.h"

// objects
std::vector<Object*> SceneBuilder::objects = std::vector<Object*>();

// destructor
SceneBuilder::~SceneBuilder()
{
	// deleting the objects in the lists
	for (Object * obj : objects)
		delete obj;
	
	objects.clear();
}

void SceneBuilder::Initialize()
{
	using namespace florp::game;
	using namespace florp::graphics;
	
	std::string currScene = "main";

	auto* scene = SceneManager::RegisterScene(currScene);
	SceneManager::SetCurrentScene(currScene);

	MeshData data = ObjLoader::LoadObj("monkey.obj", glm::vec4(1.0f));

	Shader::Sptr shader = std::make_shared<Shader>();
	shader->LoadPart(ShaderStageType::VertexShader, "shaders/lighting.vs.glsl");
	// shader->LoadPart(ShaderStageType::FragmentShader, "shaders/blinn-phong.fs.glsl");
	shader->LoadPart(ShaderStageType::FragmentShader, "shaders/blinn-phong-multi.fs.glsl");
	// shader->LoadPart(ShaderStageType::FragmentShader, "shaders/post/ex/greyscale.fs.glsl");
	  
	shader->Link();

	// Standard material value setting
	Material::Sptr mat = std::make_shared<Material>(shader);
	
	// // shaders/blinn-phong.fs.glsl
	// mat->Set("a_LightPos", { 2, 0, 4 });
	// mat->Set("a_LightColor", { 1.0f, 1.0f, 1.0f });
	// mat->Set("a_AmbientColor", { 1.0f, 1.0f, 1.0f });
	// mat->Set("a_AmbientPower", 0.1f);
	// mat->Set("a_LightSpecPower", 0.5f);
	// mat->Set("a_LightShininess", 256.0f);
	// mat->Set("a_LightAttenuation", 1.0f / 100.0f);

	// mat->Set("a_Lights[0].Pos", { 2, 0, 0 });
	// mat->Set("a_Lights[0].Color", { 1.0f, 0.0f, 1.0f });
	// mat->Set("a_Lights[0].Attenuation", 1.0f / 100.0f);
	// mat->Set("a_Lights[1].Pos", { -2, 0, 0 });
	// mat->Set("a_Lights[1].Color", { 0.0f, 1.0f, 0.0f });
	// mat->Set("a_Lights[1].Attenuation", 1.0f / 100.0f);
	// mat->Set("a_EnabledLights", 2);

	// light behaviour
	LightBehaviour lb1(mat, 3); // enabling 2 lights

	// creating the lights
	lb1.m_Lights[0] = Light(mat, 0, glm::vec3(4.0F, -2.0F, 0.0F), glm::vec3(1.0F, 0.0F, 0.0F), 1.0F / 100.0F);
	lb1.m_Lights[1] = Light(mat, 1, glm::vec3(0.0F, 2.0F, 0.0F), glm::vec3(0.0F, 1.0F, 0.0F), 1.0F / 100.0F);
	lb1.m_Lights[2] = Light(mat, 2, glm::vec3(-4.0F, -2.0F, 0.0F), glm::vec3(0.0F, 0.0F, 1.0F), 1.0F / 100.0F); 

	// default object. It doesn't use the object class, so it was removed.
	// monkey (default)
	// {
	// 	// the monkey's position
	// 	// glm::vec3 pos(0, 0, -14);
	// 	glm::vec3 pos(0, 0.0F, -14);
	// 
	// 	entt::entity test = scene->CreateEntity();
	// 	scene->AddBehaviour<SampleBehaviour>(test, glm::vec3(-45.0F, 95.0F, 30.0F)); // rotates in degrees per second
	// 	
	// 	// light behaviour
	// 	LightBehaviour lb(mat, 3); // enabling 2 lights
	// 
	// 	lb.theta = 0.1F; // rotation factor of the lights (in degrees)
	// 	scene->AddBehaviour<LightBehaviour>(test, lb); // adding the light behaviour
	// 	
	// 	RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(test);
	// 	renderable.Mesh = MeshBuilder::Bake(data);
	// 	
	// 
	// 	renderable.Material = mat;
	// 	Transform& t = scene->Registry().get<Transform>(test);
	// 	 
	// 	t.SetPosition(pos);
	// }

	// Adding in Objects
	Object* obj = nullptr; // object
	ObjectBehaviour objBehaviour; // behaviour

	// the far left placement on the top row and bottom row.
	glm::vec3 rowTop(-9.0, 8, -25);
	glm::vec3 rowBottom(-9.0, -8, -25);

	// the offset used to position objects a fixed distance away from one another.
	glm::vec3 offset1(3.5F, 0.0F, 0.0F);

	// most effects were put into their own shaders first before being combined.
	// as such any object that doesn't have the mode change when the number keys are pressed has a specialized shader.

	// 1 - no lighting
	{
		// object creation
		obj = new Object("monkey.obj", glm::vec4(1.0F), "shaders/shader.vs.glsl", "shaders/shader.fs.glsl");
		obj->CreateEntity(currScene);
	 
		Material::Sptr mat = obj->GetMaterial();

		// behaviour
		objBehaviour = ObjectBehaviour(
			rowTop + glm::vec3(offset1.x * objects.size(), offset1.y * objects.size(), offset1.z * objects.size()),
			glm::vec3(0, 0, 0),
			glm::vec3(1, 1, 1),
			glm::vec3(2, -5, 19)
		);

		scene->AddBehaviour<ObjectBehaviour>(obj->GetEntity(), objBehaviour);
		objects.push_back(obj);
	}
	
	// 2 - ambient only 	
	{
		obj = new Object("monkey.obj", glm::vec4(1.0F), "shaders/lighting.vs.glsl", "shaders/ambient.fs.glsl");
		obj->CreateEntity(currScene);

		Material::Sptr mat = obj->GetMaterial();

		// object behaviour
		objBehaviour = ObjectBehaviour(
			rowTop + glm::vec3(offset1.x * objects.size(), offset1.y * objects.size(), offset1.z * objects.size()),
			glm::vec3(0, 0, 0),
			glm::vec3(1, 1, 1),
			glm::vec3(-5, -3, 1)
		);
		
		// ambient values
		mat->Set("a_AmbientColor", { 0.4F, 0.3F, 0.8F });
		mat->Set("a_AmbientPower", 0.3F);

		scene->AddBehaviour<ObjectBehaviour>(obj->GetEntity(), objBehaviour);
		objects.push_back(obj);
	}

	// 3 - specular only
	{
		obj = new Object("monkey.obj", glm::vec4(1.0F), "shaders/lighting.vs.glsl", "shaders/specular.fs.glsl");
		obj->CreateEntity(currScene);
		 
		Material::Sptr mat = obj->GetMaterial();
		
		// object behaviour
		objBehaviour = ObjectBehaviour(
			rowTop + glm::vec3(offset1.x * objects.size(), offset1.y * objects.size(), offset1.z * objects.size()),
			glm::vec3(0, 0, 0),
			glm::vec3(1, 1, 1),
			glm::vec3(-5, -53, 12)
		);

		// light behaviour
		LightBehaviour lbx(mat, 3);

		lbx.m_Lights[0] = Light(mat, 0, glm::vec3(4.0F, -2.0F, 0.0F), glm::vec3(1.0F, 0.0F, 0.0F), 1.0F / 100.0F);
		lbx.m_Lights[1] = Light(mat, 1, glm::vec3(0.0F, 2.0F, 0.0F), glm::vec3(0.0F, 1.0F, 0.0F), 1.0F / 100.0F);
		lbx.m_Lights[2] = Light(mat, 2, glm::vec3(-4.0F, -2.0F, 0.0F), glm::vec3(0.0F, 0.0F, 1.0F), 1.0F / 100.0F);

		// no ambient components are used.
		// mat->Set("a_AmbientColor", { 0.0F, 0.0F, 0.0F });
		// mat->Set("a_AmbientPower", 0.5F);
		mat->Set("a_MatShininess", 255.0F); 

		scene->AddBehaviour<ObjectBehaviour>(obj->GetEntity(), objBehaviour);
		scene->AddBehaviour<LightBehaviour>(obj->GetEntity(), lbx);
		objects.push_back(obj);
	}

	// X - Rim Light
	// it's easier to see without extra effects, but this wasn't requested, so it's not part of the combined shader.
	{
		obj = new Object("monkey.obj", glm::vec4(1.0F), "shaders/lighting.vs.glsl", "shaders/rim.fs.glsl");
		obj->CreateEntity(currScene);  

		// material and behaviour
		Material::Sptr rimMat = obj->GetMaterial();
		LightBehaviour lbx(rimMat, 3);  

		lbx.m_Lights[0] = Light(rimMat, 0, glm::vec3(4.0F, -2.0F, 0.0F), glm::vec3(1.0F, 0.0F, 0.0F), 1.0F / 100.0F);
		lbx.m_Lights[1] = Light(rimMat, 1, glm::vec3(0.0F, 2.0F, 0.0F), glm::vec3(0.0F, 1.0F, 0.0F), 1.0F / 100.0F);
		lbx.m_Lights[2] = Light(rimMat, 2, glm::vec3(-4.0F, -2.0F, 0.0F), glm::vec3(0.0F, 0.0F, 1.0F), 1.0F / 100.0F);
		
		// rim size for lighting; no ambient values are set.
		rimMat->Set("a_RimSize", 0.3F);

		objBehaviour = ObjectBehaviour(
			rowTop + glm::vec3(offset1.x * objects.size(), offset1.y * objects.size(), offset1.z * objects.size()),
			glm::vec3(0, 0, 0),
			glm::vec3(1, 1, 1),
			glm::vec3(5, 14, 10)
		); 

		scene->AddBehaviour<ObjectBehaviour>(obj->GetEntity(), objBehaviour);
		scene->AddBehaviour<LightBehaviour>(obj->GetEntity(), lbx);
		objects.push_back(obj);
	} 
	 
	// 4 - Rim Light + Specular
	{
		obj = new Object("monkey.obj", glm::vec4(1.0F), "shaders/lighting.vs.glsl", "shaders/blinn-phong-multi-rim.fs.glsl");
		obj->CreateEntity(currScene);

		// material and behaviour
		Material::Sptr rimMat = obj->GetMaterial();
		LightBehaviour lb2(rimMat, 3);

		lb2.m_Lights[0] = Light(rimMat, 0, glm::vec3(4.0F, -2.0F, 0.0F), glm::vec3(1.0F, 0.0F, 0.0F), 1.0F / 100.0F);
		lb2.m_Lights[1] = Light(rimMat, 1, glm::vec3(0.0F, 2.0F, 0.0F), glm::vec3(0.0F, 1.0F, 0.0F), 1.0F / 100.0F);
		lb2.m_Lights[2] = Light(rimMat, 2, glm::vec3(-4.0F, -2.0F, 0.0F), glm::vec3(0.0F, 0.0F, 1.0F), 1.0F / 100.0F);

		// ambient is set to 0 since it shouldn't be used.
		rimMat->Set("a_RimSize", 0.4F);
		rimMat->Set("a_AmbientColor", { 0.0F, 0.0F, 0.0F });
		rimMat->Set("a_AmbientPower", 0.4F);
		rimMat->Set("a_MatShininess", 165.0F);

		objBehaviour = ObjectBehaviour(
			rowTop + glm::vec3(offset1.x * objects.size(), offset1.y * objects.size(), offset1.z * objects.size()),
			glm::vec3(0, 0, 0),
			glm::vec3(1, 1, 1),
			glm::vec3(5, 14, 10)
		);

		scene->AddBehaviour<ObjectBehaviour>(obj->GetEntity(), objBehaviour);
		scene->AddBehaviour<LightBehaviour>(obj->GetEntity(), lb2);
		objects.push_back(obj);
	}

	// 5 - Ambient + Specular + Rim
	{
		obj = new Object("monkey.obj", glm::vec4(1.0F), "shaders/lighting.vs.glsl", "shaders/blinn-phong-multi-rim.fs.glsl");
		obj->CreateEntity(currScene);

		// material and behaviour
		Material::Sptr rimMat = obj->GetMaterial();
		LightBehaviour lbx(rimMat, 3);

		lbx.m_Lights[0] = Light(rimMat, 0, glm::vec3(4.0F, -2.0F, 0.0F), glm::vec3(1.0F, 0.0F, 0.0F), 1.0F / 100.0F);
		lbx.m_Lights[1] = Light(rimMat, 1, glm::vec3(0.0F, 2.0F, 0.0F), glm::vec3(0.0F, 1.0F, 0.0F), 1.0F / 100.0F);
		lbx.m_Lights[2] = Light(rimMat, 2, glm::vec3(-4.0F, -2.0F, 0.0F), glm::vec3(0.0F, 0.0F, 1.0F), 1.0F / 100.0F);

		// setting the values
		rimMat->Set("a_RimSize", 0.7F);
		rimMat->Set("a_AmbientColor", { 0.2F, 0.5F, 0.8F });
		rimMat->Set("a_AmbientPower", 0.6F);
		rimMat->Set("a_MatShininess", 150.0F);

		objBehaviour = ObjectBehaviour(
			rowTop + glm::vec3(offset1.x * objects.size(), offset1.y * objects.size(), offset1.z * objects.size()),
			glm::vec3(0, 0, 0),
			glm::vec3(1, 1, 1),
			glm::vec3(28, -5, 19)
		);

		scene->AddBehaviour<ObjectBehaviour>(obj->GetEntity(), objBehaviour);
		scene->AddBehaviour<LightBehaviour>(obj->GetEntity(), lbx);
		objects.push_back(obj);
	}

	// 6 - Diffuse Texture Warp
	{
		obj = new Object("monkey.obj", glm::vec4(1.0F), "shaders/lighting.vs.glsl", "shaders/texture-warp.fs.glsl");
		obj->CreateEntity(currScene);

		Material::Sptr twMat = obj->GetMaterial();
		LightBehaviour lbx(twMat, 3);

		// lights and texture wrap
		lbx.m_Lights[0] = Light(twMat, 0, glm::vec3(4.0F, -2.0F, 0.0F), glm::vec3(1.0F, 0.0F, 0.0F), 1.0F / 100.0F);
		lbx.m_Lights[1] = Light(twMat, 1, glm::vec3(0.0F, 2.0F, 0.0F), glm::vec3(0.0F, 1.0F, 0.0F), 1.0F / 100.0F);
		lbx.m_Lights[2] = Light(twMat, 2, glm::vec3(-4.0F, -2.0F, 0.0F), glm::vec3(0.0F, 0.0F, 1.0F), 1.0F / 100.0F);
		twMat->Set("a_Albedo", Texture2D::LoadFromFile("images/saturation-gradient.png"));

		twMat->Set("a_AmbientColor", { 0.25F, 0.34F, 0.79F });
		twMat->Set("a_AmbientPower", 0.6F);
		// twMat->Set("a_MatShininess", 150.0F); // no shininess since this is just diffuse.

		objBehaviour = ObjectBehaviour(
			rowBottom + glm::vec3(offset1.x * 0, offset1.y * 0, offset1.z * 0),
			glm::vec3(0, 0, 0),
			glm::vec3(1, 1, 1),
			glm::vec3(-4, 2, 17)
		);

		scene->AddBehaviour<ObjectBehaviour>(obj->GetEntity(), objBehaviour);
		scene->AddBehaviour<LightBehaviour>(obj->GetEntity(), lbx);
		objects.push_back(obj);
	}

	// 7 - Specular Texture Warp
	{
		obj = new Object("monkey.obj", glm::vec4(1.0F), "shaders/lighting.vs.glsl", "shaders/texture-warp.fs.glsl");
		obj->CreateEntity(currScene);

		// material and behaviour
		Material::Sptr twMat = obj->GetMaterial();
		LightBehaviour lbx(twMat, 3);

		lbx.m_Lights[0] = Light(twMat, 0, glm::vec3(0.0F, -2.0F, 0.0F), glm::vec3(0.88F, 1.0F, 0.0F), 1.0F / 100.0F);
		lbx.m_Lights[1] = Light(twMat, 1, glm::vec3(-4.0F, 2.0F, 0.0F), glm::vec3(0.3F, 0.4F, 0.1F), 1.0F / 100.0F);
		lbx.m_Lights[2] = Light(twMat, 2, glm::vec3(0.0F, -2.0F, 0.0F), glm::vec3(0.1F, 0.0F, 0.3F), 1.0F / 100.0F);
		twMat->Set("a_Albedo", Texture2D::LoadFromFile("images/saturation-gradient.png"));

		twMat->Set("a_AmbientColor", { 0.441F, 0.104F, 0.959F });
		twMat->Set("a_AmbientPower", 0.2F);
		twMat->Set("a_MatShininess", 220.0F); // specular now included.

		// behaviour
		objBehaviour = ObjectBehaviour(
			rowBottom + glm::vec3(offset1.x * 1, offset1.y * 1, offset1.z * 1),
			glm::vec3(0, 0, 0),
			glm::vec3(1, 1, 1),
			glm::vec3(-12, 1, -25)
		);

		scene->AddBehaviour<ObjectBehaviour>(obj->GetEntity(), objBehaviour);
		scene->AddBehaviour<LightBehaviour>(obj->GetEntity(), lbx);
		objects.push_back(obj);
	}
	
	// 8 - Colour Grading (Warm) - LookUp Table
	{
		obj = new Object("monkey.obj", glm::vec4(1.0F), "shaders/lighting.vs.glsl", "shaders/lut.fs.glsl");
		obj->CreateEntity(currScene);

		Material::Sptr lutMat = obj->GetMaterial();
		LightBehaviour lutLb(lutMat, 3);
		
		lutLb.m_Lights[0] = Light(lutMat, 0, glm::vec3(4.0F, -2.0F, 0.0F), glm::vec3(1.0F, 0.0F, 0.0F), 1.0F / 100.0F);
		lutLb.m_Lights[1] = Light(lutMat, 1, glm::vec3(0.4F, 4.0F, 1.0F), glm::vec3(0.0F, 1.0F, 0.0F), 1.0F / 100.0F);
		lutLb.m_Lights[2] = Light(lutMat, 2, glm::vec3(-4.0F, -2.0F, 0.0F), glm::vec3(0.0F, 0.0F, 1.0F), 1.0F / 100.0F);
		 
		// setting the values
		lutMat->Set("a_AmbientColor", { 0.2F, 0.9F, 0.42F });
		lutMat->Set("a_AmbientPower", 0.3F); 
		lutMat->Set("a_MatShininess", 210.0F);
		  
		// lookup table
		LookUpTable lut("luts/ICG_2 - ASN01 - Warm Grading.CUBE"); 
		
		// setting the Texture3D for the shader.
		Texture3D::Sptr txt3D = Texture3D::LoadFromLookUpTable(lut);
		lutMat->Set("a_LUT", txt3D);

		objBehaviour = ObjectBehaviour(
			rowBottom + glm::vec3(offset1.x * 2, offset1.y * 2, offset1.z * 2),
			glm::vec3(0, 0, 0),
			glm::vec3(1, 1, 1),
			glm::vec3(-9, 12, 47)
		);
		 

		scene->AddBehaviour<ObjectBehaviour>(obj->GetEntity(), objBehaviour);
		scene->AddBehaviour<LightBehaviour>(obj->GetEntity(), lutLb);
		objects.push_back(obj);
	}

	// 9 - Colour Grading (Cool) - LookUp Table
	{
		obj = new Object("monkey.obj", glm::vec4(1.0F), "shaders/lighting.vs.glsl", "shaders/lut.fs.glsl");
		obj->CreateEntity(currScene);

		Material::Sptr lutMat = obj->GetMaterial();
		LightBehaviour lutLb(lutMat, 3);

		lutLb.m_Lights[0] = Light(lutMat, 0, glm::vec3(4.0F, -2.0F, 0.0F), glm::vec3(1.0F, 0.0F, 0.0F), 1.0F / 100.0F);
		lutLb.m_Lights[1] = Light(lutMat, 1, glm::vec3(0.4F, 4.0F, 1.0F), glm::vec3(0.0F, 1.0F, 0.0F), 1.0F / 100.0F);
		lutLb.m_Lights[2] = Light(lutMat, 2, glm::vec3(-4.0F, -2.0F, 0.0F), glm::vec3(0.0F, 0.0F, 1.0F), 1.0F / 100.0F);

		lutMat->Set("a_AmbientColor", { 0.221F, 0.555F, 0.429F });
		lutMat->Set("a_AmbientPower", 0.7F);
		lutMat->Set("a_MatShininess", 205.0F);

		// lookup table
		LookUpTable lut("luts/ICG_2 - ASN01 - Cool Grading.CUBE");

		// setting the Texture3D for the shader.
		Texture3D::Sptr txt3D = Texture3D::LoadFromLookUpTable(lut);
		lutMat->Set("a_LUT", txt3D);

		objBehaviour = ObjectBehaviour(
			rowBottom + glm::vec3(offset1.x * 3, offset1.y * 3, offset1.z * 3),
			glm::vec3(0, 0, 0),
			glm::vec3(1, 1, 1),
			glm::vec3(-1, 16, -30)
		);

		// behaviour
		scene->AddBehaviour<ObjectBehaviour>(obj->GetEntity(), objBehaviour);
		scene->AddBehaviour<LightBehaviour>(obj->GetEntity(), lutLb);
		objects.push_back(obj);
	}

	// 0 - Colour Grading (Custom) - LookUp Table
	{
		obj = new Object("monkey.obj", glm::vec4(1.0F), "shaders/lighting.vs.glsl", "shaders/lut.fs.glsl");
		obj->CreateEntity(currScene);

		Material::Sptr lutMat = obj->GetMaterial();
		LightBehaviour lutLb(lutMat, 3);

		lutLb.m_Lights[0] = Light(lutMat, 0, glm::vec3(5.0F, -2.2F, 1.0F), glm::vec3(1.0F, 0.0F, 0.0F), 1.0F / 100.0F);
		lutLb.m_Lights[1] = Light(lutMat, 1, glm::vec3(0.6F, 5.0F, 2.0F), glm::vec3(0.0F, 1.0F, 0.0F), 1.0F / 100.0F);
		lutLb.m_Lights[2] = Light(lutMat, 2, glm::vec3(-5.0F, -2.2F, 1.0F), glm::vec3(0.0F, 0.0F, 1.0F), 1.0F / 100.0F);

		lutMat->Set("a_AmbientColor", { 0.111F, 0.652F, 0.742F });
		lutMat->Set("a_AmbientPower", 0.3F);
		lutMat->Set("a_MatShininess", 210.0F);

		// lookup table
		LookUpTable lut("luts/ICG_2 - ASN01 - Custom Grading.CUBE");
		 
		// setting the texture
		Texture3D::Sptr txt3D = Texture3D::LoadFromLookUpTable(lut);
		lutMat->Set("a_LUT", txt3D);

		objBehaviour = ObjectBehaviour(
			rowBottom + glm::vec3(offset1.x * 4, offset1.y * 4, offset1.z * 4),
			glm::vec3(0, 0, 0),
			glm::vec3(1, 1, 1),
			glm::vec3(-12, 5, 33)
		);

		// setting the behaviours
		scene->AddBehaviour<ObjectBehaviour>(obj->GetEntity(), objBehaviour);
		scene->AddBehaviour<LightBehaviour>(obj->GetEntity(), lutLb);
		objects.push_back(obj);
	}

	// X - Blinn-Phong Lighting	
	{
		obj = new Object("monkey.obj", glm::vec4(1.0F), "shaders/lighting.vs.glsl", "shaders/blinn-phong-multi.fs.glsl");
		// obj = new Object("monkey.obj", glm::vec4(1.0F), "shaders/lighting.vs.glsl", "shaders/blinn-phong-effects.fs.glsl");
		obj->CreateEntity(currScene);
		
		// material
		Material::Sptr bpMat = obj->GetMaterial();
	
		// object behaviour
		objBehaviour = ObjectBehaviour(
			rowBottom + glm::vec3(offset1.x * 5, offset1.y * 5, offset1.z * 5),
			glm::vec3(0, 0, 0),
			glm::vec3(1, 1, 1),
			glm::vec3(-15, 5, 59)
		);
	
		// light behaviour
		LightBehaviour lbx(bpMat, 3);
		lbx.theta = 0.005F;

		lbx.m_Lights[0] = Light(bpMat, 0, objBehaviour.position + glm::vec3(4.0F, -2.0F, 0.0F), glm::vec3(1.0F, 0.2F, 0.1F), 1.0F / 100.0F);
		lbx.m_Lights[1] = Light(bpMat, 1, objBehaviour.position + glm::vec3(0.0F, 2.0F, 0.0F), glm::vec3(0.1F, 1.0F, 0.1F), 1.0F / 100.0F);
		lbx.m_Lights[2] = Light(bpMat, 2, objBehaviour.position + glm::vec3(-4.0F, -2.0F, 0.0F), glm::vec3(0.1F, 0.2F, 1.0F), 1.0F / 100.0F);
	
		bpMat->Set("a_AmbientColor", { 0.05F, 0.1F, 0.4F });
		bpMat->Set("a_AmbientPower", 0.6F);
		bpMat->Set("a_MatShininess", 200.0F);

		// obj->SetPosition(glm::vec3(0, 0, -14) + glm::vec3(0, 0, -1 * i));
		scene->AddBehaviour<ObjectBehaviour>(obj->GetEntity(), objBehaviour);
		scene->AddBehaviour<LightBehaviour>(obj->GetEntity(), lbx);
		objects.push_back(obj); 
	}

	// X - All (Togglable effects)
	{
		obj = new Object("monkey.obj", glm::vec4(1.0F), "shaders/lighting.vs.glsl", "shaders/blinn-phong-effects.fs.glsl");
		obj->CreateEntity(currScene);
	
		Material::Sptr mat = obj->GetMaterial();
	
		// object behaviour
		objBehaviour = ObjectBehaviour(
			glm::vec3(0.0F, 0.0F, -13),
			glm::vec3(-5, 90, -5),
			glm::vec3(1, 1, 1),
			glm::vec3(-9, 22, -54)
		);
	    
		// light behaviour
		LightBehaviour lbx(mat, 3);
	
		lbx.m_Lights[0] = Light(mat, 0, objBehaviour.position + glm::vec3(4.0F, -2.0F, 0.0F), glm::vec3(1.0F, 0.2F, 0.1F), 1.0F / 100.0F);
		lbx.m_Lights[1] = Light(mat, 1, objBehaviour.position + glm::vec3(0.0F, 2.0F, 0.0F), glm::vec3(0.1F, 1.0F, 0.1F), 1.0F / 100.0F);
		lbx.m_Lights[2] = Light(mat, 2, objBehaviour.position + glm::vec3(-4.0F, -2.0F, 0.0F), glm::vec3(0.1F, 0.2F, 1.0F), 1.0F / 100.0F);
	
		mat->Set("a_AmbientColor", { 0.05F, 0.1F, 0.4F });
		mat->Set("a_AmbientPower", 0.6F);
		mat->Set("a_MatShininess", 200.0F);

		// for modes 4, and 5
		mat->Set("a_RimSize", 0.5F);

		// for modes 6 and 7
		mat->Set("a_Albedo", Texture2D::LoadFromFile("images/saturation-gradient.png"));
		
		// for modes 8, 9, 0
		Texture3D::Sptr txtr = nullptr;

		txtr = Texture3D::LoadFromFile("luts/ICG_2 - ASN01 - Warm Grading.CUBE");
		mat->Set("a_LUT_Warm", txtr); // warm

		txtr = Texture3D::LoadFromFile("luts/ICG_2 - ASN01 - Cool Grading.CUBE");
		mat->Set("a_LUT_Cool", txtr); // cool

		txtr = Texture3D::LoadFromFile("luts/ICG_2 - ASN01 - Custom Grading.CUBE");
		mat->Set("a_LUT_Custom", txtr); // custom


		/*
		 * The lighting mode for the given object.
		 *** 1 - No Lighting
		 *** 2 - Ambient Lighting
		 *** 3 - Specular Lighting
		 *** 4 - Specular + Rim
		 *** 5 - Ambient + Specular + Rim
		 *** 6 - Diffuse Warp/Ramp
		 *** 7 - Specular Warp/Ramp
		 *** 8 - Colour Grading Warm
		 *** 9 - Colour Grading Cool
		 *** 0 - Colour Grading Custom Effect
		 *** X - Blinn-Phong (Unmatched Value)
		*/
		mat->Set("a_Mode", -1); 
	
		// obj->SetPosition(glm::vec3(0, 0, -14) + glm::vec3(0, 0, -1 * i));
		scene->AddBehaviour<ObjectBehaviour>(obj->GetEntity(), objBehaviour);
		scene->AddBehaviour<LightBehaviour>(obj->GetEntity(), lbx);
		objects.push_back(obj);
	}


	// camera setup and more
	{
		// used to determine the size of the application window.
		florp::app::Application* app = florp::app::Application::Get();

		// The color buffer should be marked as shader readable, so that we generate a texture for it
		RenderBufferDesc mainColor = RenderBufferDesc();
		mainColor.ShaderReadable = true;
		mainColor.Attachment = RenderTargetAttachment::Color0;
		mainColor.Format = RenderTargetType::Color24;
		// The depth attachment does not need to be a texture (and would cause issues since the format is DepthStencil)
		RenderBufferDesc depth = RenderBufferDesc();
		depth.Attachment = RenderTargetAttachment::DepthStencil;
		depth.Format = RenderTargetType::DepthStencil;
		// Our main frame buffer needs a color output, and a depth output
		FrameBuffer::Sptr buffer = std::make_shared<FrameBuffer>(app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight());
		buffer->AddAttachment(mainColor);
		buffer->AddAttachment(depth);
		buffer->Validate();


		entt::entity camera = scene->CreateEntity();
		CameraComponent& cam = scene->Registry().assign<CameraComponent>(camera);
		cam.Projection = glm::perspective(glm::radians(60.0f), 1.0f, 0.01f, 1000.0f);
		cam.Buffer = buffer;
		cam.IsMainCamera = true;

		// rotates the camera (removed)
		// scene->AddBehaviour<SampleBehaviour>(camera, glm::vec3(0.0F, 0.0F, 25.0F));
		Transform& t = scene->Registry().get<Transform>(camera);


		t.SetPosition(glm::vec3(0.0f, 0.0f, -5.0f));
		// t.SetPosition(glm::vec3(2.0f, -4.5f, -2.0f)); // original

		// camera behaviour, which allows for keyboad input.
		CameraBehaviour camBvh = CameraBehaviour();
		camBvh.translationSpeed = glm::vec3(6.0F, 6.0F, 6.0F);
		camBvh.rotationSpeed = glm::vec3(20.0F, 20.0F, 20.0F);
		 
		scene->AddBehaviour<CameraBehaviour>(camera, camBvh);
	}
}
