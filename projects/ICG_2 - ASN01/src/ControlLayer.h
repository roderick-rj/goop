/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: used for keyboard inputs.
 * References: none
 */

// applies functionality for different keyboard inputs.
#pragma once
#include "InputLayer.h"

#include <GLFW/glfw3.h>
#include <florp/app/Window.h>
#include <florp/game/Transform.h>
#include <entt.hpp>

// controls the camera by taking overriding keyboard functions in the InputLayer class.
// this was seperated from the InputLayer class due to being specialized to this assignment.
class ControlLayer : public InputLayer
{
public:

	// constructor
	ControlLayer() = default;

	// destructor
	~ControlLayer() = default;

	// called when a key has been pressed
	void KeyPressed(GLFWwindow* window, int key) override;
	
	// called when a key is being held down
	void KeyHeld(GLFWwindow* window, int key) override;
	
	// called when a key has been released
	void KeyReleased(GLFWwindow* window, int key) override;

	// gets the translation direction
	static glm::ivec3 GetTranslationDirection();

	// gets the rotation direction
	static glm::ivec3 GetRotationDirection();

	// overrides the update loop
	void Update() override;

private:
	// (x, y, z) movement direction
	static glm::ivec3 t_Dir;

	// (x, y, z) rotation direction
	static glm::ivec3 r_Dir;

	// determines what the new mode is. The modes are numbered off as 0 through 9.
	int mode = -1;

	// switches the mode if 'true'
	bool modeSwitch = false;

protected:
};
