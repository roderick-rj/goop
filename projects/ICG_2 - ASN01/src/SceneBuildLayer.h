/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: builds objects for the scene.
 * References: none
*/

#pragma once
#include "florp/app/ApplicationLayer.h"
#include "Object.h"
#include <vector>

class SceneBuilder : public florp::app::ApplicationLayer {
public:

	// destructor
	~SceneBuilder();

	void Initialize() override;

	// list of objects, which is static so that they can be updated.
	static std::vector<Object*> objects;

private:
	

protected:
};
