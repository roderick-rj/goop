/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: object class.
 * References: none
*/

// class for making objects
#pragma once

#include <florp/graphics/ObjLoader.h>
#include <florp/graphics/MeshData.h>
#include <florp/graphics/Shader.h>

#include <florp/game/Scene.h>
#include <florp/game/SceneManager.h>
#include <florp/game/Material.h>
#include <florp/game/BehaviourLayer.h>

#include "ObjectBehaviour.h"

// object class
class Object
{
public:
	// the file path, colour, vertex shader, and fragment shader
	Object(std::string filePath, glm::vec4 baseColour, std::string vertShader, std::string fragShader);

	// creates an obejct with a base colour and provided material
	Object(std::string filePath, glm::vec4 baseColour, florp::game::Material::Sptr mat);

	// returns the mesh.
	const florp::graphics::MeshData& GetMesh() const;

	// returns the base colour
	const glm::vec4 GetColor() const;

	// creates the entity
	void CreateEntity(std::string sceneName);

	// returns the name of the scene
	std::string GetSceneName() const;

	// adds a behaviour
	template<typename T>
	void AddBehaviour(T behaviour);

	// gets the entity
	const entt::entity& GetEntity() const;

	// returns the material
	const florp::game::Material::Sptr& GetMaterial() const;

private:
	// the mesh for the object
	florp::graphics::MeshData mesh;

	// the material
	florp::game::Material::Sptr material;

	// base colour
	glm::vec4 baseColor;

	// the entity
	entt::entity entity;

	// the object behaviour, which handles transformations.
	ObjectBehaviour objectTransform;

	// sets the scene name
	std::string sceneName = "";

protected:
};
