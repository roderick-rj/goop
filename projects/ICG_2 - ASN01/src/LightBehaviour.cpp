/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: behaviour for moving lights around.
*/

#include <GLM/mat4x4.hpp>
#include <florp/game/Transform.h>
#include <GLM\gtc\matrix_transform.hpp>
#include "LightBehaviour.h"

// maximum amount of lights
const int LightBehaviour::MAX_LIGHTS = 16;

// constructor
LightBehaviour::LightBehaviour(florp::game::Material::Sptr& mat, int enabledLights) : 
	IBehaviour(), m_EnabledLights(enabledLights)// , material(mat)
{
	mat->Set("a_EnabledLights", m_EnabledLights);
};


// adds a light.
// void LightBehaviour::AddLight(unsigned int index, const Light& light)
// {
// 	m_Lights[index] = light;
// }

// transforms the lights
void LightBehaviour::Update(entt::entity entity)
{
	// gets the transform of the entity, but doesn't use it.
	auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);

	glm::mat4 posMatEX = glm::mat4(1.0F); // position matrix
	glm::mat4 resMatEX; // resulting matrix

	// updating the lights
	for (int i = 0; i < m_EnabledLights; i++)
	{
		// filling the position matrix
		posMatEX[0][3] = m_Lights[i].GetPosition().x;
		posMatEX[1][3] = m_Lights[i].GetPosition().y;
		posMatEX[2][3] = m_Lights[i].GetPosition().z;
		posMatEX[3][3] = 1;

		// rotating the light
		resMatEX = glm::rotate(posMatEX, glm::radians(theta), glm::vec3(0.0F, 0.0F, 1.0F));

		// saving the new position
		m_Lights[i].SetPosition(
			glm::vec3(
				resMatEX[0][3],
				resMatEX[1][3],
				resMatEX[2][3]
			)
		);

		// reseting the matrices
		posMatEX = glm::mat4(1.0F);
		resMatEX = glm::mat4(1.0F);
	}
}