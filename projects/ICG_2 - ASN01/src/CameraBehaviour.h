/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: class for camera transformations.
 * References: none
 */
// used to translate and rotate the camera
#pragma once

#include <florp/game/IBehaviour.h>
#include <GLM/vec3.hpp>

// behaviour class
class CameraBehaviour : public florp::game::IBehaviour {
public:
	CameraBehaviour() = default;

	// update loop
	void Update(entt::entity entity) override;

	// the translation speed of the camera, which is the incrementer for each axis.
	glm::vec3 translationSpeed;

	// the rotation speed of the camera, which is the incrementer for each axis.
	glm::vec3 rotationSpeed;

private:

protected:

};