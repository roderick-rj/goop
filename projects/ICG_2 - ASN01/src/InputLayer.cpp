/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: takes in user input for the project.
 * References:
 */

// user input class
#include "InputLayer.h"
#include <florp/app/Application.h>

// holds a reference to the input layer so that it can access its down, held, and up input functions.
InputLayer* thisLayer = nullptr;

// called when a mouse button event is recorded
void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
	// if the input layer does not exist
	if (thisLayer == nullptr)
		return;

	// calling specific functions
	switch (action) {
	case GLFW_PRESS: // button pressed
		thisLayer->MouseButtonPressed(window, button);
		break;

	case GLFW_REPEAT: // button held
		thisLayer->MouseButtonHeld(window, button);
		break;

	case GLFW_RELEASE: // button released
		thisLayer->MouseButtonReleased(window, button);
		break;
	}
}



// keyboard callback - called for keyboard inputs
// KeyCallback(Window, Keyboard Key, Platform-Specific Scancode, Key Action, and Modifier Bits)
void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	// if the input layer does not exist
	if (thisLayer == nullptr)
		return;

	// calling specific functions
	switch (action)
	{
	case GLFW_PRESS: // key has been pressed
		thisLayer->KeyPressed(window, key);
		break;

	case GLFW_REPEAT: // key is held down
		thisLayer->KeyHeld(window, key);
		break;

	case GLFW_RELEASE: // key has been released
		thisLayer->KeyReleased(window, key);
		break;
	}
}

// gets the window for the application, and saves a reference to the input layer.
InputLayer::InputLayer() : ApplicationLayer()
{
	thisLayer = this; // saves reference to itself
	myWindow = florp::app::Application::Get()->GetWindow()->GetHandle();
	glfwSetKeyCallback(myWindow, KeyCallback);
}

// mouse button has been pressed. This is unused.
void InputLayer::MouseButtonPressed(GLFWwindow* window, int button)
{
}

// mouse button is being held. This is unused.
void InputLayer::MouseButtonHeld(GLFWwindow* window, int button)
{
}

// mouse button has been released. This is unused.
void InputLayer::MouseButtonReleased(GLFWwindow* window, int button)
{
}

// key has been pressed
void InputLayer::KeyPressed(GLFWwindow* window, int key)
{
	// the ControlLayer overrides this function and applies the input functionality for this assignment.
}

// key is being held
void InputLayer::KeyHeld(GLFWwindow* window, int key)
{
	// the ControlLayer overrides this function and applies the input functionality for this assignment.
}

// key hs been released
void InputLayer::KeyReleased(GLFWwindow* window, int key)
{
	// the ControlLayer overrides this function and applies the input functionality for this assignment.
}

// update loop
void InputLayer::Update()
{
	// no unique functionality.
	ApplicationLayer::Update();
}
