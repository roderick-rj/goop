/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: post processing effects for the scene. This is unchanged from what's in the tutorial framework.
 * References: none
*/

#pragma once
#include <florp\app\Application.h>
#include <florp/graphics/Mesh.h>
#include <florp\graphics\Shader.h>
#include <FrameBuffer.h>

class PostLayer : public florp::app::ApplicationLayer
{
public:
	PostLayer();
	
	virtual void OnWindowResize(uint32_t width, uint32_t height) override;

	virtual void PostRender() override;
protected:
	// FrameBuffer::Sptr myMainFrameBuffer; // no longer needed
	
	florp::graphics::Mesh::Sptr myFullscreenQuad;
	
	struct PostPass {
		florp::graphics::Shader::Sptr Shader;
		FrameBuffer::Sptr Output;
	};
	std::vector<PostPass> myPasses;
};