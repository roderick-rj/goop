/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: behaviour for transforming objects.
 * References: none
*/

#pragma once

#include <florp/game/IBehaviour.h>
#include <Logging.h>
#include <florp/game/Transform.h>
#include <florp/game/SceneManager.h>
#include <florp/app/Timing.h>
#include <imgui.h>

// forward declares the object class
class Object;

// object behaviour class
class ObjectBehaviour : public florp::game::IBehaviour {
public:
	// constructor
	ObjectBehaviour();

	// position, orientation (degrees), scale, and rotation factor (degrees)
	ObjectBehaviour(glm::vec3 aPos, glm::vec3 aOrin = glm::vec3(0, 0, 0), glm::vec3 aScale = glm::vec3(1, 1, 1), glm::vec3 aRot = glm::vec3(0, 0, 0));
	 
	// destructor
	virtual ~ObjectBehaviour() = default;

	// update loop
	virtual void Update(entt::entity entity) override;

	// position
	glm::vec3 position = glm::vec3();

	// rotation (degrees)
	glm::vec3 orientation = glm::vec3();

	// the rotation factor of the object, which gets applied each frame.
	glm::vec3 rotationFactor = glm::vec3();

	// scale
	glm::vec3 scale = glm::vec3(1.0F, 1.0F, 1.0F);
	
private:
	

protected:

};




