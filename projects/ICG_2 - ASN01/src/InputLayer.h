/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: takes in user input for the project.
 * References:
 */

// used for user input
#pragma once
#include <florp/app/ApplicationLayer.h>
#include <GLFW/glfw3.h>

// getting user input gets added like any other layer.
class InputLayer : public florp::app::ApplicationLayer
{
public:
	// constructor
	InputLayer();

	// called when a mouse button has been pressed
	virtual void MouseButtonPressed(GLFWwindow* window, int button);

	// called when a mouse button is being held.
	virtual void MouseButtonHeld(GLFWwindow* window, int button);

	// called when a mouse button has been pressed
	virtual void MouseButtonReleased(GLFWwindow* window, int button);

	// called when a key has been pressed
	virtual void KeyPressed(GLFWwindow* window, int key);

	// called when a key is being held down
	virtual void KeyHeld(GLFWwindow* window, int key);

	// called when a key has been released
	virtual void KeyReleased(GLFWwindow* window, int key);

	// overriding the update loop
	void Update() override;

private:

	// saves a reference to the window.
	GLFWwindow * myWindow = nullptr;

protected:
};
