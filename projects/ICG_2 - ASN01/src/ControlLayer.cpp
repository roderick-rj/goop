/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: used for keyboard inputs.
 * References: none
 */

#include "ControlLayer.h"

#include <florp\app\Timing.h>
#include "SceneBuildLayer.h"

// the default translation.
glm::ivec3 ControlLayer::t_Dir = glm::vec3(0.0, 0.0, 0.0);

// the default rotation
glm::ivec3 ControlLayer::r_Dir = glm::vec3(0.0, 0.0, 0.0);

// key pressed
void ControlLayer::KeyPressed(GLFWwindow* window, int key)
{
	switch (key)
	{
		// CAMERA CONTROLS
		// TRANSLATIONS
	case GLFW_KEY_W: // y-direction up
		t_Dir[1] = -1;
		break;

	case GLFW_KEY_S: // y-direction down
		t_Dir[1] = 1;
		break;
		
	case GLFW_KEY_A: // x-direction left
		t_Dir[0] = 1;
		break;

	case GLFW_KEY_D: // x-direction right
		t_Dir[0] = -1;
		break;

	case GLFW_KEY_Q: // z-direction backward
		t_Dir[2] = 1;
		break;

	case GLFW_KEY_E: // z-direction forward
		t_Dir[2] = -1;
		break;

		// ROTATIONS
	case GLFW_KEY_UP: // y-direction +
		r_Dir[0] = -1;
		break;

	case GLFW_KEY_DOWN: // y-direction -
		r_Dir[0] = 1;
		break;

	case GLFW_KEY_LEFT: // x-direction +
		r_Dir[1] = -1;
		break;

	case GLFW_KEY_RIGHT: // x-direction -
		r_Dir[1] = 1;
		break;

	case GLFW_KEY_PAGE_UP: // z-direction -
		r_Dir[2] = -1;
		break;

	case GLFW_KEY_PAGE_DOWN: // z-direction +
		r_Dir[2] = 1;
		break;

		// MODE CONTROLS
	case GLFW_KEY_1: // 1 - no lighting
		mode = 1;
		modeSwitch = true;
		break;

	case GLFW_KEY_2: // 2 - ambient lighting only
		mode = 2;
		modeSwitch = true;
		break;

	case GLFW_KEY_3: // 3 - specular lighting only
		mode = 3;
		modeSwitch = true;
		break;

	case GLFW_KEY_4: // 4 - specular + rim lighting
		mode = 4;
		modeSwitch = true;
		break;

	case GLFW_KEY_5: // 5 - ambient + specular + rim
		mode = 5;
		modeSwitch = true;
		break;

	case GLFW_KEY_6: // 6 - diffuse wrap/ramp
		mode = 6;
		modeSwitch = true;
		break;

	case GLFW_KEY_7: // 7 - specular warp/ramp
		mode = 7;
		modeSwitch = true;
		break;

	case GLFW_KEY_8: // 8 - colour grading warm
		mode = 8;
		modeSwitch = true;
		break;

	case GLFW_KEY_9: // 9 - colour grading cool
		mode = 9;
		modeSwitch = true;
		break;

	case GLFW_KEY_0: // 0 - colour grading custom
		mode = 0;
		modeSwitch = true;
		break;
		
	default: // X - blinn-phong (no unique mode)
		mode = -1;
		modeSwitch = true;
	}
}

// key held
void ControlLayer::KeyHeld(GLFWwindow* window, int key)
{
	switch (key)
	{
		// CAMERA CONTROLS
		// TRANSLATIONS
	case GLFW_KEY_W: // y-direction up
		t_Dir[1] = -1;
		break;

	case GLFW_KEY_S: // y-direction down
		t_Dir[1] = 1;
		break;

	case GLFW_KEY_A: // x-direction left
		t_Dir[0] = 1;
		break;

	case GLFW_KEY_D: // x-direction right
		t_Dir[0] = -1;
		break;

	case GLFW_KEY_Q: // z-direction backward
		t_Dir[2] = 1;
		break;

	case GLFW_KEY_E: // z-direction forward
		t_Dir[2] = -1;
		break;

		// ROTATIONS
	case GLFW_KEY_UP: // y-direction +
		r_Dir[0] = -1;
		break;

	case GLFW_KEY_DOWN: // y-direction -
		r_Dir[0] = 1;
		break;

	case GLFW_KEY_LEFT: // x-direction +
		r_Dir[1] = -1;
		break;

	case GLFW_KEY_RIGHT: // x-direction -
		r_Dir[1] = 1;
		break;

	case GLFW_KEY_PAGE_UP: // z-direction -
		r_Dir[2] = -1;
		break;

	case GLFW_KEY_PAGE_DOWN: // z-direction +
		r_Dir[2] = 1;
		break;
	}
}

// key released
void ControlLayer::KeyReleased(GLFWwindow* window, int key)
{
	switch (key)
	{
	// CAMERA CONTROLS
	// TRANSLATIONS
	// y-axis movement
	case GLFW_KEY_W:
	case GLFW_KEY_S:
		t_Dir[1] = 0;
		break;

	// x-axis movement
	case GLFW_KEY_A:
	case GLFW_KEY_D:
		t_Dir[0] = 0;
		break;

	// z-axis movement
	case GLFW_KEY_Q:
	case GLFW_KEY_E:
		t_Dir[2] = 0;
		break;

	// ROTATIONS
	// y-axis rotation
	case GLFW_KEY_UP:
	case GLFW_KEY_DOWN:
		r_Dir[0] = 0;
		break;

	// x-axis rotation
	case GLFW_KEY_LEFT:
	case GLFW_KEY_RIGHT:
		r_Dir[1] = 0;
		break;

	// z-axis rotation
	case GLFW_KEY_PAGE_UP:
	case GLFW_KEY_PAGE_DOWN:
		r_Dir[2] = 0;
		break;
	}
}

// get the translation direction
glm::ivec3 ControlLayer::GetTranslationDirection() { return t_Dir; }

// gets the rotation direction
glm::ivec3 ControlLayer::GetRotationDirection() { return r_Dir; }

// update loop
void ControlLayer::Update()
{
	using namespace florp::app;
	 
	// if the mode has to be switched, then said change is applied to all objects.
	if (modeSwitch)
	{
		// switching the mode for all objects
		// not all objects have the a_Mode parameter, but there is no distinction to know which ones do and do not have it.
		for (Object* obj : SceneBuilder::objects)
		{
			obj->GetMaterial()->Set("a_Mode", mode);
		}

		// mode switch complete
		modeSwitch = false;
	}
}
