/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: a light class to add lights to a material.
 * References: none
*/

#pragma once

#include <GLM/vec2.hpp>
#include <GLM/vec3.hpp>
#include <GLM/vec4.hpp>
#include <florp/game/Material.h>

#include <string>

// light
struct Light
{
public:
    // constructor
    Light() = default;

    Light(florp::game::Material::Sptr mat, int index, glm::vec3 pos, glm::vec3 clr, float atten);

    // Taken out since there is only one ambient colour and ambient power value for the object.

    // copies a light's values to the material stored in the object.
    // Light(const Light&);

    // // gets the ambient colour of the material
    // glm::vec3 GetAmbientColor() const;
    // 
    // // sets the ambient color
    // void SetAmbientColor(glm::vec3 ambClr);
    // 
    // // gets the ambient power of the material
    // float GetAmbientPower() const;
    // 
    // // sets the ambient power
    // void SetAmbientPower(float ambPwr);
    // 
    // // gets the mateiral shininess of the material
    // float GetShininess() const;
    // 
    // // sets the shininess
    // void SetShininess(float shine);

    // gets the index
    int GetIndex() const;

    // sets the index
    void SetIndex(int index);

    // gets the position
    glm::vec3 GetPosition() const;

    // sets the position
    void SetPosition(glm::vec3 newPos);

    // gets the color
    glm::vec3 GetLightColor() const;

    // sets the color
    void SetLightColor(glm::vec3 newClr);

    // gets the attenuation of the material
    float GetAttenuation() const;

    // sets the attenuation of the material
    void SetAttenuation(float atten);

    // copies all the values of a light to the current material.
    void CopyLight(const Light& light);

    // copies the values of a light to the saved material.
    // if the index shouldn't be the one saved in the light, a new index is provided.
    void CopyLight(const Light& light, int index);

    // sets the material
   //  void SetMaterial(florp::game::Material::Sptr& mat);

    // gets the material
    // florp::game::Material::Sptr& GetMaterial() const;

    // translates the light
    void Translate(glm::vec3 translate);

    // the material
    florp::game::Material::Sptr material = nullptr;
    
    // maximum amount of lights
    static const int MAX_LIGHTS;

private:
    // the ambient color
    glm::vec3 ambientColor;

    // the ambient power
    float ambientPower;

    // material shininess
    float matShininess;

    glm::vec3 position{}; // (x, y, z)
    glm::vec3 color{}; // rgb
    float attenuation = 1.0F; // lighting attenuation
    int index = 0; // the light's index.
    
};
