/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: main function that starts the application
 * References: none
*/

#include "florp/app/Application.h"
#include "florp/game/BehaviourLayer.h"
#include "SceneBuildLayer.h"
#include "florp/game/ImGuiLayer.h"
#include <RenderLayer.h>
#include <PostLayer.h>

// #include "InputLayer.h"
#include "ControlLayer.h" // includes InputLayer.h

int main()
{
	// Create our application
	florp::app::Application* app = new florp::app::Application();
	 
	// Set up our layers
	app->AddLayer<florp::game::BehaviourLayer>();
	app->AddLayer<florp::game::ImGuiLayer>();
	app->AddLayer<SceneBuilder>();
	app->AddLayer<RenderLayer>();
	// app->AddLayer<InputLayer>(); // its child 'ControlLayer' gets added instead.
	app->AddLayer<ControlLayer>(); // used for movement; inherits from InputLayer

	app->AddLayer<PostLayer>(); // add it as the very last layer.
	app->Run();

	return 0;
} 
