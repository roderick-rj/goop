/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: behaviour for adding lights to a material, and rotating them if desired.
 * References: none
*/

#pragma once
#include <florp/game/IBehaviour.h>
#include <Logging.h>
#include <florp/game/Transform.h>
#include <florp/game/SceneManager.h>
#include <florp/app/Timing.h>
#include <imgui.h>

#include <iostream>
#include <florp/game/Material.h>
#include "Light.h"

// lighting behaviour
class LightBehaviour : public florp::game::IBehaviour {
public:
	// constructor - takes in the material and the lights enabled on it.
	LightBehaviour(florp::game::Material::Sptr& mat, int enabledLights);
	
	// destructor
	virtual ~LightBehaviour() = default;

	// EDIT: taken out because the material doesn't get saved in the behaviour.
	// adds a light to the current material. The material saved in the provided light is ignored.
	// void AddLight(unsigned int index, const Light & light);

	// update for lighting behaviours
	virtual void Update(entt::entity entity) override;

	// rotation factor (in degrees)
	float theta = 0;

	// light array
	Light m_Lights[16];

	// maximum light count
	static const int MAX_LIGHTS;

private:
	// amount of enabled lights
	int m_EnabledLights = 0;

	// the material
	// florp::game::Material::Sptr material;
};