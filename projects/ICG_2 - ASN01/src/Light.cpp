/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: a light class to add lights to a material.
 * References: none
*/

#include "Light.h"

const int Light::MAX_LIGHTS = 16;

// constructor
Light::Light(florp::game::Material::Sptr mat, int index, glm::vec3 pos, glm::vec3 clr, float atten)
{
    // setting the values
    material = mat;
    SetIndex(index);
    SetPosition(pos);
    SetLightColor(clr);
    SetAttenuation(atten);
}

// Taken out since each material only has one ambient colour and ambient power value.

// gets the ambient colour of the material
// glm::vec3 Light::GetAmbientColor() const { return ambientColor; }
// 
// // sets the ambient color
// void Light::SetAmbientColor(glm::vec3 ambClr)
// {
//     ambientColor = ambClr;
// 
//     if (material != nullptr)
//         material->Set("a_AmbientColor", ambientColor);
// }
// 
// // gets the ambient power
// float Light::GetAmbientPower() const { return ambientPower; }
// 
// // sets the ambient power
// void Light::SetAmbientPower(float ampPwr)
// {
//     ambientPower = ampPwr;
// 
//     if (material != nullptr)
//         material->Set("a_AmbientPower", ampPwr);
// }
// 
// // gets the shininess
// float Light::GetShininess() const { return matShininess; }
// 
// // sets the shininess
// void Light::SetShininess(float shine)
// {
//     matShininess = shine;
// 
//     if (material != nullptr)
//         material->Set("a_MatShininess", matShininess);
// }

// gets the index the light is part of.
int Light::GetIndex() const { return index; }

// sets the index
void Light::SetIndex(int index)
{
    if (index >= 0 && index < MAX_LIGHTS)
        this->index = index;
};

// gets the position
glm::vec3 Light::GetPosition() const { return position; }

// sets the position
void Light::SetPosition(glm::vec3 newPos)
{
    position = newPos;

    if (material != nullptr)
        material->Set("a_Lights[" + std::to_string(index) + "].Pos", position);
}

// gets the color
glm::vec3 Light::GetLightColor() const { return color; }

// sets the color
void Light::SetLightColor(glm::vec3 newClr)
{
    color = newClr;

    if (material != nullptr)
        material->Set("a_Lights[" + std::to_string(index) + "].Color", color);

}

// gets the attenuation
float Light::GetAttenuation() const { return attenuation; }

// sets the attenuation
void Light::SetAttenuation(float atten)
{
    attenuation = atten;

    if (material != nullptr)
        material->Set("a_Lights[" + std::to_string(index) + "].Attenuation", atten);
}

// copies the values of a light
void Light::CopyLight(const Light& light) { CopyLight(light, light.GetIndex()); }

// copies the light
void Light::CopyLight(const Light& light, int index)
{
    // sets the new index, and copies the values
    SetIndex(index);
    SetPosition(light.GetPosition());
    SetLightColor(light.GetLightColor());
    SetAttenuation(light.GetAttenuation());
}

// sets the material
// void Light::SetMaterial(florp::game::Material::Sptr& mat)
// {
//     material = mat;
// }
// 
// // gets the material
// florp::game::Material::Sptr& Light::GetMaterial() const
// {
//     return material;
// }

// translates the light
void Light::Translate(glm::vec3 translate) { SetPosition(position + translate); }