/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: used to translate and rotate the camera.
 * References: none
 */
#include "CameraBehaviour.h"

#include <florp/app/Timing.h>
#include <florp/game/SceneManager.h>
#include "ControlLayer.h"


// update loop
void CameraBehaviour::Update(entt::entity entity)
{
	// gets the delta time and the camera's transformation.
	float deltaTime = florp::app::Timing::DeltaTime;
	auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
	
	// gets the direction of rotation and translation from the ControlLayer
	glm::ivec3 t_Dir = ControlLayer::GetTranslationDirection();
	glm::ivec3 r_Dir = ControlLayer::GetRotationDirection();

	// translation and rotation
	transform.SetPosition(transform.GetLocalPosition() + translationSpeed * (glm::vec3)t_Dir * deltaTime);
	transform.SetEulerAngles(transform.GetLocalEulerAngles() + rotationSpeed * (glm::vec3)r_Dir * deltaTime);
}
