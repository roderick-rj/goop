/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: object class.
 * References: none
*/
// class for objects
#include "Object.h"

#include <florp/game/RenderableComponent.h>
#include <florp/graphics/MeshBuilder.h>

// constructor
Object::Object(std::string filePath, glm::vec4 baseColour, std::string vertShader, std::string fragShader)
{
	using namespace florp::game;
	using namespace florp::graphics;

	// loads the object.
	mesh = florp::graphics::ObjLoader::LoadObj(filePath.c_str(), baseColour);

	// shader
	Shader::Sptr shader = std::make_shared<Shader>();

	// making the shader
	shader->LoadPart(ShaderStageType::VertexShader, vertShader);
	shader->LoadPart(ShaderStageType::FragmentShader, fragShader);
	shader->Link(); 
	  
	// creating the material
	material = std::make_shared<Material>(shader); 
}
   
// constructor
Object::Object(std::string filePath, glm::vec4 baseColour, florp::game::Material::Sptr mat)
{
	using namespace florp::game;
	using namespace florp::graphics;

	// loads mesh and material
	mesh = florp::graphics::ObjLoader::LoadObj(filePath.c_str(), baseColour);
	material = mat;
}

// returns the mesh
const florp::graphics::MeshData& Object::GetMesh() const
{
	return mesh;
}

// returns the base colour
const glm::vec4 Object::GetColor() const { return baseColor; }

// creates the entity
void Object::CreateEntity(std::string sceneName)
{
	using namespace florp::game;
	using namespace florp::graphics;

	this->sceneName = sceneName;

	auto* scene = SceneManager::Get(sceneName);
	entity = scene->CreateEntity();

	// adding it as a renderable component.
	RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(entity);
	renderable.Mesh = MeshBuilder::Bake(mesh);
	renderable.Material = material;
}

// gets the scene name
std::string Object::GetSceneName() const { return sceneName; }

// gets the entity
const entt::entity& Object::GetEntity() const { return entity; }

// returns the material
const florp::game::Material::Sptr& Object::GetMaterial() const { return material; }

// adds a behaviour
template<typename T>
void Object::AddBehaviour(T behaviour)
{
	using namespace florp::game;

	auto* scene = SceneManager::Get(sceneName);
	
	scene->AddBehaviour<T>(entity, behaviour);
}