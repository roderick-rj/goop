/*
 * Name: Roderick "R.J." Montague (100701758)
 * Date: 02/04/2020
 * Description: post-processing effects. This is a hold over from the takeover assignment.
	* the vibrance processing effect is used to brighten the scene. 
	* You can RenderDoc the exe or turn the effect off in if you want to see the scene without it.
 * References:
*/

#include "PostLayer.h"
#include <florp\app\Application.h>
#include <florp\game\SceneManager.h>
 
#include "Texture3D.h"

PostLayer::PostLayer() {
	// set to 'true' if you want to enable the inversion.
	bool exInvert = false; 

	// set to 'true' to display the checkerboard pattern.
	bool exCheckerboard = false;

	// [0] = all enabled, [1] = greyscale, [2] = vibrance, [3] = sepia tone
	// any other value for exShader will disable all filters.
	int exShader = 2;

	florp::app::Application* app = florp::app::Application::Get();
	// The color buffer should be marked as shader readable, so that we generate a texture for it
	RenderBufferDesc mainColor = RenderBufferDesc();
	mainColor.ShaderReadable = true;
	mainColor.Attachment = RenderTargetAttachment::Color0;
	mainColor.Format = RenderTargetType::Color24;
	// TODO: Create our fullscreen mesh
	
	// Create our fullscreen quad mesh
	{
		// a quad
		float vert[] = {
		-1.0f, -1.0f, 0.0f, 0.0f,
		1.0f, -1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f
		};
		uint32_t indices[] = {
		0, 1, 2,
		1, 3, 2
		};
		florp::graphics::BufferLayout layout = {
		{ "inPosition", florp::graphics::ShaderDataType::Float2 },
		{ "inUV", florp::graphics::ShaderDataType::Float2 }
		};
		myFullscreenQuad = std::make_shared<florp::graphics::Mesh>(vert, 4, layout, indices, 6);
	}

	// colour inversion
	if(exInvert)
	{
		auto shader = std::make_shared<florp::graphics::Shader>();
		shader->LoadPart(florp::graphics::ShaderStageType::VertexShader, "shaders/post/post.vs.glsl");
		shader->LoadPart(florp::graphics::ShaderStageType::FragmentShader, "shaders/post/invert.fs.glsl");
		shader->Link();

		// we don't need depth information for this.
		// shader->SetUniform("xCheckerSize", 2);
		// shader->SetUniform("xCheckerColor", glm::vec3(0.0f, 0.0f, 0.0f));
		// Each pass gets it's own copy of the frame buffer to avoid pipeline stalls

		auto output = std::make_shared<FrameBuffer>(app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight());
		output->AddAttachment(mainColor);
		output->Validate();
		// Add the pass to the post processing stack
		myPasses.push_back({ shader, output });
	}

	// Adding a post process layer. 
	if(exCheckerboard)
	{
		auto shader = std::make_shared<florp::graphics::Shader>();
		shader->LoadPart(florp::graphics::ShaderStageType::VertexShader, "shaders/post/post.vs.glsl");
		shader->LoadPart(florp::graphics::ShaderStageType::FragmentShader, "shaders/post/checker.fs.glsl");
		shader->Link();
		shader->SetUniform("xCheckerSize", 16);
		shader->SetUniform("xCheckerColor", glm::vec3(1.0F, 0.0F, 0.69F));

		// we don't need depth information for this.
		// shader->SetUniform("xCheckerSize", 2);
		// shader->SetUniform("xCheckerColor", glm::vec3(0.0f, 0.0f, 0.0f));
		// Each pass gets it's own copy of the frame buffer to avoid pipeline stalls

		auto output = std::make_shared<FrameBuffer>(app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight());
		output->AddAttachment(mainColor);
		output->Validate();
		// Add the pass to the post processing stack
		myPasses.push_back({ shader, output });
	}

	// EX1: extra post processor - greyscale
	if(exShader == 0 || exShader == 1)
	{
		auto shader = std::make_shared<florp::graphics::Shader>();
		shader->LoadPart(florp::graphics::ShaderStageType::VertexShader, "shaders/post/post.vs.glsl");
		shader->LoadPart(florp::graphics::ShaderStageType::FragmentShader, "shaders/post/greyscale.fs.glsl");
		shader->Link();

		auto output = std::make_shared<FrameBuffer>(app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight());
		output->AddAttachment(mainColor);
		output->Validate();
		
		// Add the pass to the post processing stack
		myPasses.push_back({ shader, output });
	}

	// EX2: extra post processor - vibrance
	if (exShader == 0 || exShader == 2)
	{
		auto shader = std::make_shared<florp::graphics::Shader>();
		shader->LoadPart(florp::graphics::ShaderStageType::VertexShader, "shaders/post/post.vs.glsl");
		shader->LoadPart(florp::graphics::ShaderStageType::FragmentShader, "shaders/post/vibrant.fs.glsl"); 
		shader->Link();


		auto output = std::make_shared<FrameBuffer>(app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight());
		output->AddAttachment(mainColor);
		output->Validate();

		// Add the pass to the post processing stack
		myPasses.push_back({ shader, output });
	}
	 
	// EX3: extra post processor - sepia tone
	if (exShader == 0 || exShader == 3)
	{
	
		auto shader = std::make_shared<florp::graphics::Shader>();
		shader->LoadPart(florp::graphics::ShaderStageType::VertexShader, "shaders/post/post.vs.glsl");
		shader->LoadPart(florp::graphics::ShaderStageType::FragmentShader, "shaders/post/sepiatone.fs.glsl");
		shader->Link();
		// shader->SetUniform("xTime", 0.5);
		// shader->SetUniform("xImage2", florp::graphics::Texture2D::LoadFromFile("bliss_windows_xp.png"));

		auto output = std::make_shared<FrameBuffer>(app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight());
		output->AddAttachment(mainColor);
		output->Validate();

		// Add the pass to the post processing stack
		myPasses.push_back({ shader, output });
	}

	// I was unable to add a lookup table as a post-processing effect.
	// LookUp Table
	// {
	// 
	// 	auto shader = std::make_shared<florp::graphics::Shader>();
	// 	shader->LoadPart(florp::graphics::ShaderStageType::VertexShader, "shaders/post/post.vs.glsl");
	// 	shader->LoadPart(florp::graphics::ShaderStageType::FragmentShader, "shaders/post/lut-post.fs.glsl");
	// 	shader->Link();
	// 	// shader->SetUniform("xTime", 0.5);
	// 	// shader->SetUniform("xImage2", florp::graphics::Texture2D::LoadFromFile("bliss_windows_xp.png"));
	// 
	// 	Texture3D::Sptr txt3D = Texture3D::LoadFromFile("luts/ICG_2 - Lookup Table (Ver. 2).CUBE");
	// 	txt3D->Bind(3);
	// 	shader->SetUniform("a_LUT", 3);
	//  
	// 	auto output = std::make_shared<FrameBuffer>(app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight());
	// 	output->AddAttachment(mainColor); 
	// 	output->Validate(); 
	// 
	// 	// Add the pass to the post processing stack
	// 	myPasses.push_back({ shader, output });
	// }
}

void PostLayer::OnWindowResize(uint32_t width, uint32_t height)
{
	//myMainFrameBuffer->Resize(width, height);
	for (auto& pass : myPasses)
		pass.Output->Resize(width, height);
}

void PostLayer::PostRender() {
	// We grab the application singleton to get the size of the screen
	florp::app::Application* app = florp::app::Application::Get();
	FrameBuffer::Sptr mainBuffer = CurrentRegistry().ctx<FrameBuffer::Sptr>();
	glDisable(GL_DEPTH_TEST);
	// The last output will start as the output from the rendering
	FrameBuffer::Sptr lastPass = mainBuffer;

	// We'll iterate over all of our render passes
	for (const PostPass& pass : myPasses) {
		// We'll bind our post-processing output as the current render target and clear it
		pass.Output->Bind(RenderTargetBinding::Draw);
		glClear(GL_COLOR_BUFFER_BIT);
		// Set the viewport to be the entire size of the passes output
		glViewport(0, 0, pass.Output->GetWidth(), pass.Output->GetHeight());
		// Use the post processing shader to draw the fullscreen quad
		pass.Shader->Use();
		lastPass->GetAttachment(RenderTargetAttachment::Color0)->Bind(0);
		pass.Shader->SetUniform("xImage", 0);
		pass.Shader->SetUniform("xScreenRes", glm::ivec2(app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight()));
		// pass.Shader->SetUniform("a_LUT", 3);
		myFullscreenQuad->Draw();
		
		// Unbind the output pass so that we can read from it
		pass.Output->UnBind();
		// Update the last pass output to be this passes output
		lastPass = pass.Output;
	}

	// Bind the last buffer we wrote to as our source for read operations
	lastPass->Bind(RenderTargetBinding::Read);
	// Copies the image from lastPass into the default back buffer
	FrameBuffer::Blit({ 0, 0, lastPass->GetWidth(), lastPass->GetHeight() },
		{ 0, 0, app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight() }, BufferFlags::All, florp::graphics::MagFilter::Nearest);
	// Unbind the last buffer from read operations, so we can write to it again later
	lastPass->UnBind();
}