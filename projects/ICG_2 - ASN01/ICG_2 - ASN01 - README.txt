INFR 2350: Intermediate Computer Graphics
Assignment 1: Lighting & Colour Correction

Name: Roderick "R.J." Montague
Student Number: 100701758
Due Date: 02/03/2020

The objects have all been given their own rotation values, which cannot be changed without altering the code.
References for the code are mainly in the report.

**********
CONTROLS:
**********
The controls for the program are detailed below.

----------
MODES
----------
The modes are switched using the number keys 0 through 9. Hitting any key that does not have an established function will reset the lighting to be blinn-phong.
Only the large centre object is effected by these controls. The surrounding objects are fixed to sho certain lighting functions.
The corresponding function of each key is shown below.
	- 1 - No Lighting
	- 2 - Ambient Lighting
	- 3 - Specular Lighting
	- 4 - Specular + Rim
	- 5 - Ambient + Specular + Rim
	- 6 - Diffuse Warp/Ramp
 	- 7 - Specular Warp/Ramp
	- 8 - Colour Grading Warm
	- 9 - Colour Grading Cool
 	- 0 - Colour Grading Custom Effect
 	- X - Blinn-Phong (Unmatched Value)
		- Any key that doesn't have a set function will trigger this.

----------
CAMERA
----------
Controls for the camera are detailed below.

-- MOVEMENT --
The 'WASDQE' keys are used for moving the Camera.
The controls are described based on how they appear, along with their change along a given axis. 
Due to the orientation of the camera, the way the directions are described may be contrary to the expected result from the axis change.
	- W - Up (y-)
	- S - Down (y+)
	- A - Left (x+)
	- D - Right (x-)
	- Q - Zoom Out (z+)
	- E - Zoom In (z-)

-- ORIENTATION --
The arrow keys, Page Up, and Page Down are used to change the orientation of the camera.
Just like with the camera movement, the way the controls are described by contrast from controls expectations.
	- UP ARROW: Rotate Upwards (x-)
	- DOWN ARROW: Rotate Downwards (x+)
	- LEFT ARROW: Rotate Left (y-)
	- RIGHT ARROW: Rotate Right (x+)
	- PAGE UP: Counter Clockwise (z-)
	- PAGE DOWN: Clockwise (z+)
