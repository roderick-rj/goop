// Name: Roderick "R.J." Montague (100701758)
// Date: 02/04/2020
// Description: blinn-phong shader that has all modes for the assignmetn.

// blinn-phong shader that toggles between the 11 modes for the assignment (10 modes + default)
// this is based on blinn-phong-multi, which was provided in a tutorial.
#version 410

layout(location = 0) in vec4 inColor;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inWorldPos;
layout(location = 3) in vec2 inUV;

layout(location = 0) out vec4 outColor;

/*
 * The lighting mode for the given object.
 *** 1 - No Lighting
 *** 2 - Ambient Lighting
 *** 3 - Specular Lighting
 *** 4 - Specular + Rim
 *** 5 - Ambient + Specular + Rim
 *** 6 - Diffuse Warp/Ramp
 *** 7 - Specular Warp/Ramp
 *** 8 - Colour Grading Warm
 *** 9 - Colour Grading Cool
 *** 0 - Colour Grading Custom Effect
 *** X - Blinn-Phong (Unmatched Value)
*/
uniform int a_Mode;

uniform vec3  a_CameraPos;

uniform vec3  a_AmbientColor;
uniform float a_AmbientPower;
uniform float a_MatShininess;

const int MAX_LIGHTS = 16;
struct Light{
	vec3  Pos;
	vec3  Color;
	float Attenuation;
};
uniform Light a_Lights[MAX_LIGHTS];
uniform int a_EnabledLights;

// the size of the rim (rim functionality only)
uniform float a_RimSize;

// the albedo for a 1D texture
uniform sampler2D a_Albedo;

// lookup tables
uniform sampler3D a_LUT_Warm;
uniform sampler3D a_LUT_Cool;
uniform sampler3D a_LUT_Custom;

// resolves a point light using complete blinn-phong (ambient, diffuse, specular)
vec3 ResolvePointLight(Light light, vec3 norm) {
	// Determine the direction from the position to the light
	vec3 toLight = light.Pos - inWorldPos;

	// Determine the distance to the light (used for attenuation later)
	float distToLight = length(toLight);
	// Normalize our toLight vector
	toLight = normalize(toLight);

	// Determine the direction between the camera and the pixel
	vec3 viewDir = normalize(a_CameraPos - inWorldPos);

	// Calculate the halfway vector between the direction to the light and the direction to the eye
	vec3 halfDir = normalize(toLight + viewDir);

	// Our specular power is the angle between the the normal and the half vector, raised
	// to the power of the light's shininess
	float specPower = pow(max(dot(norm, halfDir), 0.0), a_MatShininess);

	// Finally, we can calculate the actual specular factor
	vec3 specOut = specPower * light.Color;

	// Calculate our diffuse factor, this is essentially the angle between
	// the surface and the light
	float diffuseFactor = max(dot(norm, toLight), 0);
	// Calculate our diffuse output
	vec3  diffuseOut = diffuseFactor * light.Color;

	// We will use a modified form of distance squared attenuation, which will avoid divide
	// by zero errors and allow us to control the light's attenuation via a uniform
	float attenuation = 1.0 / (1.0 + light.Attenuation * pow(distToLight, 2));

	return attenuation * (diffuseOut + specOut);
}


// diffuse only
vec3 ResolvePointLightDiffuse(Light light, vec3 norm) {
	// Determine the direction from the position to the light
	vec3 toLight = light.Pos - inWorldPos;

	// Determine the distance to the light (used for attenuation later)
	float distToLight = length(toLight);
	// Normalize our toLight vector
	toLight = normalize(toLight);

	// Determine the direction between the camera and the pixel
	vec3 viewDir = normalize(a_CameraPos - inWorldPos);

	// Calculate the halfway vector between the direction to the light and the direction to the eye
	vec3 halfDir = normalize(toLight + viewDir);

	// Calculate our diffuse factor, this is essentially the angle between
	// the surface and the light
	float diffuseFactor = max(dot(norm, toLight), 0);
	// Calculate our diffuse output
	vec3  diffuseOut = diffuseFactor * light.Color;

	// We will use a modified form of distance squared attenuation, which will avoid divide
	// by zero errors and allow us to control the light's attenuation via a uniform
	float attenuation = 1.0 / (1.0 + light.Attenuation * pow(distToLight, 2));

	return attenuation * diffuseOut;
}

// specular only
vec3 ResolvePointLightSpecular(Light light, vec3 norm) {
	// Determine the direction from the position to the light
	vec3 toLight = light.Pos - inWorldPos;

	// Determine the distance to the light (used for attenuation later)
	float distToLight = length(toLight);

	// Normalize our toLight vector
	toLight = normalize(toLight);

	// Determine the direction between the camera and the pixel
	vec3 viewDir = normalize(a_CameraPos - inWorldPos);

	// Calculate the halfway vector between the direction to the light and the direction to the eye
	vec3 halfDir = normalize(toLight + viewDir);

	// Our specular power is the angle between the the normal and the half vector, raised
	// to the power of the light's shininess
	float specPower = pow(max(dot(norm, halfDir), 0.0), a_MatShininess);

	// Finally, we can calculate the actual specular factor
	vec3 specOut = specPower * light.Color;

	// We will use a modified form of distance squared attenuation, which will avoid divide
	// by zero errors and allow us to control the light's attenuation via a uniform
	float attenuation = 1.0 / (1.0 + light.Attenuation * pow(distToLight, 2));

	return attenuation * (specOut);
}

void main() {
	// Re-normalize our input, so that it is always length 1
	vec3 norm = normalize(inNormal);
	
	// Our ambient is simply the color times the ambient power
	vec3 result = a_AmbientColor * a_AmbientPower;

	// used for specular-only results
	// if the mode is equal to 5, ambient is included.
	vec3 resultSpec = (a_Mode == 5) ? result : vec3(0, 0, 0);

	// used for diffuse-only reuslts
	vec3 resultDiffuse = vec3(0, 0, 0);

	// Iterate over all the lights and sum their influence on the final result
	// regular point lights
	for (int i = 0; (i < a_EnabledLights) && (i < MAX_LIGHTS); i++) 
	{
		// solves for all three variations of 'ResolvePointLight'.
		result += ResolvePointLight(a_Lights[i], norm);
		resultSpec += ResolvePointLightSpecular(a_Lights[i], norm);
		resultDiffuse += ResolvePointLightDiffuse(a_Lights[i], norm);
	}


	// Multiply the lighting by the object's color
	result = result * inColor.rgb;
	resultSpec = resultSpec * inColor.rgb;
	resultDiffuse = resultDiffuse * inColor.rgb;

	// TODO: gamma correction
	// not for this assignement.

	// used for some functions.
	float edge = 0; // used for rim lighting
	vec4 newColor = vec4(0, 0, 0, 0); // used for lookup tables
	vec4 albedo = vec4(0, 0, 0, 0); // used for texture warp

	// calculating the edge, which is used for rim lighting
	// finding the pixel in reference to the camera. The closer the value is to 0, the closer it is to the edge.
	edge = dot(normalize(a_CameraPos - inWorldPos), inNormal);

	// clamping the edge to 0 and 1.
	edge = clamp(edge, 0, 1);

	// Write the output
	switch(a_Mode)
	{
	case 1: // No Lighting
		outColor = inColor;
		break;
		
	case 2: // Ambient Lighting Only
		outColor = vec4((a_AmbientColor * a_AmbientPower) * inColor.rgb, inColor.a);
		break;

	case 3: // Specular Lighting
		outColor = vec4(resultSpec, inColor.a);
		break;

	case 4: // Specular + Rim
	case 5: // Ambient + Specular + Rim
		// when using only specular, the ambient component isn't added in, which was taken care of earlier.

		// setting the colour for the edge, and writing output.
		if(edge < a_RimSize)
		{
			// brighter the closer to the edge the pixel is.
			outColor = vec4(result * (1 - edge), inColor.a);
		}
		else // not part of the rim.
		{
			outColor = vec4(resultSpec, inColor.a);
		}
		break;

	case 6: // Diffuse Warp/Ramp
		albedo = texture(a_Albedo, vec2(inUV.x, 0));
		result = resultDiffuse * albedo.rgb;

		outColor = vec4(result, inColor.a * albedo.a);
		break;

	case 7: // Specular Warp/Ramp
		albedo = texture(a_Albedo, vec2(inUV.x, 0));
		result = result * albedo.rgb;

		outColor = vec4(result, inColor.a * albedo.a);
		break;

	case 8: // Colour Grading Warm
		// finding the pixel value in the lookup table
		newColor = texture(a_LUT_Warm, vec3(result.r, result.g, result.b));
		outColor = vec4(newColor.rgb, inColor.a);
		break;

	case 9: // Color Grading Cool
		// finding the pixel value in the lookup table
		newColor = texture(a_LUT_Cool, vec3(result.r, result.g, result.b));
		outColor = vec4(newColor.rgb, inColor.a);
		break;

	case 0: // Colour Grading Custom Effect
		// finding the pixel value in the lookup table
		newColor = texture(a_LUT_Custom, vec3(result.r, result.g, result.b));
		outColor = vec4(newColor.rgb, inColor.a);
		break;

	default: // Blinn-Phong
		outColor = vec4(result, inColor.a);
		break;
	}
}