// Name: Roderick "R.J." Montague (100701758)
// Date: 02/04/2020
// Description: ambient lighting shader. There is no diffuse, specular, or attenuation component.

// shader that only uses ambient component of blinn-phong.
#version 410

layout(location = 0) in vec4 inColor;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inWorldPos;

layout(location = 0) out vec4 outColor;

uniform vec3  a_CameraPos;

uniform vec3  a_AmbientColor;
uniform float a_AmbientPower;


void main() {

	// finds the ambient colour by multiplying it by the ambient power.
	vec3 ambientOut = a_AmbientColor * a_AmbientPower;

	// lighting result * rgb colour.
	vec3 result = ambientOut * inColor.rgb;

	// TODO: gamma correction
	// not completed for the assignment.

	// Write the output
	outColor = vec4(result, inColor.a); // * a_ColorMultiplier;
}