// Name: Roderick "R.J." Montague (100701758)
// Date: 02/04/2020
// Description: inverts the colours of the screen. This isn't used for this assignment.

// inverts the colours of the image. This is a holdover from the takehome assignment.
#version 440
layout (location = 0) in vec2 inUV;
layout (location = 1) in vec2 inScreenCoords;

layout (location = 0) out vec4 outColor;

uniform sampler2D xImage;

void main() {
	vec4 color = texture(xImage, inUV);
	outColor = vec4(1 - color.rgb, color.a); // inversion
}