// Name: Roderick "R.J." Montague (100701758)
// Date: 02/04/2020
// Description: makes the colours of the screen brighter and more vibrant.

// filter for making a colour more vibrant. This is a holdover from the take-home assignment.
// this is actually still enabled so that the scene is made brighter.
#version 440
layout (location = 0) in vec2 inUV;
layout (location = 1) in vec2 inScreenCoords;

layout (location = 0) out vec4 outColor;

uniform sampler2D xImage;

void main() {
	// color
	vec4 color = texture(xImage, inUV);
	
	// multiplier
	float multi = 0;

	// checking for the highest colour value
	if(color.r >= color.g && color.r >= color.b)
	{
		// color.r * x = 1.0F
		multi = 1.0F / color.r;

	}
	else if(color.g >= color.r && color.g >= color.b)
	{
		// color.g * x = 1.0F
		multi = 1.0F / color.g;
	}
	else if(color.b >= color.r && color.b >= color.g)
	{
		// color.b * x = 1.0F
		multi = 1.0F / color.b;
	}

	// increasing the vibrance by multiplying it by the multifactor
	color.r = clamp(color.r * multi, 0.0F, 1.0F);
	color.g = clamp(color.g * multi, 0.0F, 1.0F);
	color.b = clamp(color.b * multi, 0.0F, 1.0F);

	outColor = color;
}