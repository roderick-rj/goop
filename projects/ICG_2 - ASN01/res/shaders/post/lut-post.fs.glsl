// Name: Roderick "R.J." Montague (100701758)
// Date: 02/04/2020
// Description: lookup table applied as a post processing effect. This doesn't actually work, but this is the way luts are supposed to be used.

// applies a lookup table as a post processing effect
// this doesn't actually work unfortunately.
#version 440
layout (location = 0) in vec2 inUV;
layout (location = 1) in vec2 inScreenCoords;

layout (location = 0) out vec4 outColor;

uniform sampler2D xImage; // image
uniform sampler3D a_LUT; // lookup table

void main() {
	vec4 color = texture(xImage, inUV);
	vec4 newColor = texture(a_LUT, vec3(color.g, color.r, color.b)); // getting the colour value from the lookup table.

	outColor = vec4(newColor.rgb, color.a);
}