// Name: Roderick "R.J." Montague (100701758)
// Date: 02/04/2020
// Description: a greyscale filter for the image.

// converts an image to greyscale. This is a holdover from the take home assignment.
#version 440
layout (location = 0) in vec2 inUV;
layout (location = 1) in vec2 inScreenCoords;

layout (location = 0) out vec4 outColor;

uniform sampler2D xImage;

void main() {
	vec4 color = texture(xImage, inUV);
	float res = (color.r + color.g + color.b) / 3.0F; // takes the average of the colour values.

	// sets the new colour values to be the average of the original colour.
	outColor = vec4(res, res, res, color.a);
}