// Name: Roderick "R.J." Montague (100701758)
// Date: 02/04/2020
// Description: regular shader with no changes to the fragments.

// standard fragment shader with no interesting featureus.
// all this does is keep the object's base colour as its final colour.
#version 410

layout(location = 0) in vec4 inColor;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inWorldPos;
layout(location = 3) in vec2 inUV;

layout(location = 0) out vec4 outColor;

uniform vec3  a_CameraPos;

void main() 
{
	outColor = inColor;
}