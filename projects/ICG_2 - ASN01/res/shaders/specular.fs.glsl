// Name: Roderick "R.J." Montague (100701758)
// Date: 02/04/2020
// Description: blinn-phong, except only the specular component is used.

// Specular Lighting
// this is the same as the blinn-phong-multi.fs.glsl shader, except with the diffuse components removed.
// ambient is still included because the object is white by default, which makes it hard to see the specular effect.
#version 410

layout(location = 0) in vec4 inColor;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inWorldPos;

layout(location = 0) out vec4 outColor;

uniform vec3  a_CameraPos;

uniform vec3  a_AmbientColor;
uniform float a_AmbientPower;
uniform float a_MatShininess;

const int MAX_LIGHTS = 16;
struct Light{
	vec3  Pos;
	vec3  Color;
	float Attenuation;
};
uniform Light a_Lights[MAX_LIGHTS];
uniform int a_EnabledLights;

// this is the same as the one found in blinn-phong except with diffuse components removed.
vec3 ResolvePointLightSpecular(Light light, vec3 norm) {
	// Determine the direction from the position to the light
	vec3 toLight = light.Pos - inWorldPos;

	// Determine the distance to the light (used for attenuation later)
	float distToLight = length(toLight);

	// Normalize our toLight vector
	toLight = normalize(toLight);

	// Determine the direction between the camera and the pixel
	vec3 viewDir = normalize(a_CameraPos - inWorldPos);

	// Calculate the halfway vector between the direction to the light and the direction to the eye
	vec3 halfDir = normalize(toLight + viewDir);

	// Our specular power is the angle between the the normal and the half vector, raised
	// to the power of the light's shininess
	float specPower = pow(max(dot(norm, halfDir), 0.0), a_MatShininess);

	// Finally, we can calculate the actual specular factor
	vec3 specOut = specPower * light.Color;

	// We will use a modified form of distance squared attenuation, which will avoid divide
	// by zero errors and allow us to control the light's attenuation via a uniform
	float attenuation = 1.0 / (1.0 + light.Attenuation * pow(distToLight, 2));

	return attenuation * (specOut);
}

void main() {
	// Re-normalize our input, so that it is always length 1
	vec3 norm = normalize(inNormal);
	
	// Our ambient is simply the color times the ambient power
	vec3 result = a_AmbientColor * a_AmbientPower;

	// Iterate over all the lights and sum their influence on the final result
	for (int i = 0; (i < a_EnabledLights) && (i < MAX_LIGHTS); i++) {
		result += ResolvePointLightSpecular(a_Lights[i], norm); // uses specular and ambient only now.
	}

	// Multiply the lighting by the object's color
	result = result * inColor.rgb;

	// TODO: gamma correction
	// I didn't have time to implement it.

	// Write the output
	outColor = vec4(result, inColor.a);
}