/*
 * Name: Team Bonus Fruit
	- Kennedy Adams	(100632983)
	- Jonah Griffin	(100702748)
	- Nathan Tuck	(100708651)
	- Stephane Gagnon (100694227)
	- Roderick "R.J." Montague (100701758)
 * Date: 03/13/2020
 * Description: plays the audio for the assignment.
 * References: 
	- https://github.com/eppz/Unity.Library.eppz_easing
*/

#pragma once
#include "florp/app/ApplicationLayer.h"
#include "GLM/vec3.hpp"
#include <string>

class AudioLayer : public florp::app::ApplicationLayer
{
public:
	// initialization function
	void Initialize() override;
	
	// shutdown function
	void Shutdown() override;
	
	// these lerp functions were used for audio movement purposes.
	// only the ease-in function is used in the final version.

	// standard linear interpolation
	static float Lerp(float a, float b, float u);

	// ease in interpolation
	static float EaseInLerp(float a, float b, float u);

	// ease out interpolation
	static float EaseOutLerp(float a, float b, float u);

	// ease in and out interpolation
	static float EaseInOutLerp(float a, float b, float u);

	// update function
	void Update() override;

private:
	// the event for the sound
	std::string audioEvent = "Car Crash";

	// becomes 'true' when the audio is set to play. Becomes false when the audio is 'almost' done.
	// it's used this way because .isEventPlaying() kept returning false from the audio engine.
	bool playing = false;

	// x is left/right, y is up/down, z is in/out of the screen.
	// this is assuming that the camera orientation is its default, which it may not be.

	// starting, ending, and current position of the audio.
	const glm::vec3 START_POS = glm::vec3(-1.0F, 0.0F, -2.0F);
	const glm::vec3 END_POS = glm::vec3(15.0F, 0.0F, -2.0F);
	glm::vec3 currPos = glm::vec3();

	// 'u' (time) value for lerp.
	float u = 0.0F;

	// incrementer for the 'u' value. It is multiplied by delta time.
	const float U_INC = 0.09F;

protected:

};
