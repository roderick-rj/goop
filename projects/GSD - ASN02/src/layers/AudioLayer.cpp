/*
 * Name: Team Bonus Fruit
	- Kennedy Adams	(100632983)
	- Jonah Griffin	(100702748)
	- Nathan Tuck	(100708651)
	- Stephane Gagnon (100694227)
	- Roderick "R.J." Montague (100701758)
 * Date: 03/13/2020
 * Description: plays the audio for the assignment.
 * References:
	- https://github.com/eppz/Unity.Library.eppz_easing
*/

#include "AudioLayer.h"
#include "AudioEngine.h"
#include "florp/app/Application.h"
#include "florp/app/Window.h"
#include "florp/app/Timing.h"

// initialization
void AudioLayer::Initialize()
{
	// init the sound engine and load the master bank
	AudioEngine& audioEngine = AudioEngine::GetInstance();

	audioEngine.Init(); // getting the singleton.
	audioEngine.LoadBank("Master"); // loading the master bank

	// loading the audio event
	audioEngine.LoadEvent(audioEvent);
	// audioEngine.PlayEvent(audioEvent);

	// setting the starting position
	AudioEngine::GetInstance().SetEventPosition(audioEvent, START_POS);
}

// shutting down layer
void AudioLayer::Shutdown()
{
	AudioEngine::GetInstance().Shutdown(); // shutting down audio engine
}

// linerar interpolation
float AudioLayer::Lerp(float a, float b, float u) { return glm::mix(a, b, u); }

// ease in interpolation. The higher the exponent, the greater the ease-in and steeper the curve.
// see the reference for variations of this interpolation type.
float AudioLayer::EaseInLerp(float a, float b, float u)
{
	// y = x^n
	return glm::mix(a, b, powf(u, 5));
}

// ease out interpolation. The higher the final (u) value, the harder the curve. 
// see the reference for variations of this interpolation type.
float AudioLayer::EaseOutLerp(float a, float b, float u)
{
	// y = 1 - (1 - x)^n
	return glm::mix(a, b, 1.0F - powf(1.0F - u, 8));
}

// ease in and out interpolation.
// see the reference for variations of this interpolation type.
float AudioLayer::EaseInOutLerp(float a, float b, float u)
{
	float t = (u < 0.5F) ? 
		128.0F * (pow(u,8)) :
		0.5F + (1.0F - powf((2.0F * (1.0F - u)), 8)) / 2.0F;

	return glm::mix(a, b, t);
}

// update loop
void AudioLayer::Update()
{
	using namespace florp::app;

	// getting the values needed for the loop
	Window::Sptr window = Application::Get()->GetWindow();
	float deltaTime = Timing::DeltaTime;
	AudioEngine& audioEngine = AudioEngine::GetInstance();

	// key press
	if (window->IsKeyDown(Key::P)) // plays the event
	{
		audioEngine.PlayEvent(audioEvent); // plays/restarts event
		playing = true; // event is playing

		u = 0.0F; // time value reset
		currPos = glm::vec3(); // current position is reset (it doesn't need to be really)
	}
	

	// this is supposed to check if the event is still playing, but it always returned false, so it isn't used anymore.
	// playing = audioEngine.isEventPlaying(audioEvent);

	// if the audio is playing
	if (playing)
	{
		// the startup sound is roughly 3 seconds of actual noise (whole effect is 3.5 seconds)
		// this is where the 'easing in' takes place, as the audio source wouldn't be moving much at this stage.
		
		// the ending crash starts about 11 seconds into the audio.
		// when the crash happens, the audio source stops moving (hence the u < 1.0F), and has reached its end point.
		if (u < 1.0F) // crashed into wall
		{
			// moving the audio source
			currPos.x = EaseInLerp(START_POS.x, END_POS.x, u);
			u += U_INC * deltaTime;
			AudioEngine::GetInstance().SetEventPosition(audioEvent, currPos);
		}
		else
		{
			// playing is set to 'false' so that the values can be reset for the next play.
			// the audio isn't actually finished by this point, but the audio soruce has stopped moving.
			playing = false;
		}
	}

	// update audio engine.
	audioEngine.Update();
}
