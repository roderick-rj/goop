/*
 * Name: Team Bonus Fruit
	- Kennedy Adams	(100632983)
	- Jonah Griffin	(100702748)
	- Nathan Tuck	(100708651)
	- Stephane Gagnon (100694227)
	- Roderick "R.J." Montague (100701758)
 * Date: 03/13/2020
 * Description: the main function for the second assignment. This hasn't been changed.
 * References:
*/

// INFR: 2370 - Game Sound - Assignment 1
#include "florp/app/Application.h"
#include "florp/game/BehaviourLayer.h"
#include "florp/game/ImGuiLayer.h"
#include "layers/SceneBuildLayer.h"
#include "layers/RenderLayer.h"
#include "layers/PostLayer.h"
#include "layers/AudioLayer.h"
#include "layers/LightingLayer.h"
#include "florp/graphics/TextureCube.h"

int main()
{
	{
		// Create our application
		florp::app::Application* app = new florp::app::Application();

		// Set up our layers
		app->AddLayer<florp::game::BehaviourLayer>();
		app->AddLayer<florp::game::ImGuiLayer>();
		app->AddLayer<AudioLayer>();
		app->AddLayer<SceneBuilder>();
		app->AddLayer<RenderLayer>();
		app->AddLayer<LightingLayer>();
		app->AddLayer<PostLayer>();

		app->Run();

		delete app;
	}
		
	return 0;
} 
